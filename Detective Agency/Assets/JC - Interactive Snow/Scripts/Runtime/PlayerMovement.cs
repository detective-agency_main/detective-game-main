using UnityEngine;

namespace JC.Snow
{
    [RequireComponent(typeof (Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {
        #region Player Movement Code
        private Rigidbody rig;
        private Vector3 velMovement;
        private Vector3 initPosition;

        private void Awake()
        {
            rig = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            initPosition = transform.position;    
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                rig.AddForce(Vector3.up * 5f, ForceMode.Impulse);
            }
        }

        private void FixedUpdate()
        {
            Vector3 direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

            rig.MovePosition(Vector3.SmoothDamp(rig.position, rig.position + direction, ref velMovement, Time.deltaTime * 5));

            if(direction != Vector3.zero)
            {
                rig.MoveRotation(Quaternion.Slerp(rig.rotation, Quaternion.LookRotation(direction), velMovement.magnitude * 4 * Time.deltaTime));
            }

            if(rig.position.y <= -5f)
            {
                rig.linearVelocity = Vector3.zero;
                rig.MovePosition(initPosition);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name.Contains("Sphere"))
            {
                collision.rigidbody.AddForce((transform.forward + transform.up) * 5f, ForceMode.Impulse);
            }
        }
        #endregion
    }
}
