using System.Collections;
using UnityEngine;

namespace JC.Snow
{
    public class InteractiveGround : MonoBehaviour
    {
        [SerializeField] private GameObject trailPrefab;
        [SerializeField] private float clearTrailsTime = 10f;
        [SerializeField] private string clearTag = "Cleanable Particles";
        [SerializeField] private LayerMask layerCollision;

        private Transform trailGroup;

        private void Awake()
        {
            trailGroup = new GameObject("Trail Group").transform;
        }

        private void Start()
        {
            StartCoroutine(ClearTrails());
        }

        private IEnumerator ClearTrails()
        {
            while (true)
            {
                GameObject[] trails = GameObject.FindGameObjectsWithTag(clearTag);

                foreach (GameObject trail in trails)
                {
                    if (trail.GetComponent<ParticleSystem>().particleCount == 0 && !trail.GetComponent<ParticleSystem>().isPlaying)
                    {
                        foreach (Transform childTrail in trail.transform)
                        {
                            childTrail.SetParent(trailGroup);
                        }

                        Destroy(trail);
                    }
                }

                yield return new WaitForSeconds(clearTrailsTime);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            OnEnter(other.gameObject.layer, other.gameObject);
        }

        private void OnCollisionEnter(Collision collision)
        {
            OnEnter(collision.gameObject.layer, collision.gameObject);
        }

        private void OnEnter(int layer, GameObject obj)
        {
            if (layerCollision != (layerCollision | 1 << layer)) return;

            if (obj.GetComponentInChildren<ParticleSystem>() == null)
            {
                Transform trail = Instantiate(trailPrefab, obj.transform).transform;

                trail.GetComponent<ParticleSystem>().Play();
            }

            else
            {
                obj.GetComponentInChildren<ParticleSystem>().Play();
            }
        }

        private void OnCollisionStay(Collision collision)
        {
            OnStay(collision.gameObject.layer, collision.gameObject);
        }

        private void OnTriggerStay(Collider other)
        {
            OnStay(other.gameObject.layer, other.gameObject);
        }

        private void OnStay(int layer, GameObject obj)
        {
            if (layerCollision != (layerCollision | 1 << layer)) return;

            ParticleSystem trail = obj.GetComponentInChildren<ParticleSystem>();
            if (trail && !trail.isPlaying) trail.Play();
        }

        private void OnCollisionExit(Collision collision)
        {
            OnExit(collision.gameObject.layer, collision.gameObject);
        }

        private void OnTriggerExit(Collider other)
        {
            OnExit(other.gameObject.layer, other.gameObject);
        }

        private void OnExit(int layer, GameObject obj)
        {
            if (layerCollision != (layerCollision | 1 << layer)) return;

            ParticleSystem trail = obj.GetComponentInChildren<ParticleSystem>();

            if (trail)
            {
                trail.transform.SetParent(trailGroup);
                trail.transform.localScale = Vector3.one;
                trail.Stop();
            }
        }
    }
}

