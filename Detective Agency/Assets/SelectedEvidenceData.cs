using System.Collections;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SelectedEvidenceData : MonoBehaviour
{
    Object_InventoryImport object_Data;
    int INcroTitle = 0;
    int INcroDiscription = 0;
    string TextoutTile = "";
    string TextoutDiscription = "";
    [SerializeField] TextMeshProUGUI BottomDiscription;
    [SerializeField] TextMeshProUGUI TitleText;
    [SerializeField] Image ImageChange;
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    string TextTile = "";
    string TextBottomDiscription = "";
    public void SetInformation(Object_InventoryImport import)
    {
        object_Data = import;

        TitleText.text = "";
        BottomDiscription.text = "";

        TextTile = "Object name : " + object_Data.ObjectDataName.ToString() + "\n";
        TextBottomDiscription = "Object Clue " + object_Data.Object_Clue;

        ImageChange.sprite = object_Data.ObjectInventory_Image;

        StartCoroutine(Typeouttitle(TextTile, TitleText));
        StartCoroutine(TypeoutDiscrip(TextBottomDiscription, BottomDiscription));

    }

    IEnumerator Typeouttitle(string sentance, TextMeshProUGUI SelectedText)
    {
        yield return new WaitForSeconds(0.08f); // how quick it types out

        int countLengh = sentance.Length;

        TextoutTile += sentance[INcroTitle];

        SelectedText.text = TextoutTile;
        INcroTitle++;

        if (INcroTitle < countLengh)
        {
            StartCoroutine(Typeouttitle(sentance,SelectedText));
        }
        else
        {
            StartCoroutine(endSelfTalkDialogTitle());
        }
    }

    IEnumerator TypeoutDiscrip(string sentance, TextMeshProUGUI SelectedText)
    {
        yield return new WaitForSeconds(0.08f); // how quick it types out

        int countLengh = sentance.Length;

        TextoutDiscription += sentance[INcroDiscription];
        
        SelectedText.text = TextoutDiscription;
        INcroDiscription++;

        if (INcroDiscription < countLengh)
        {
            StartCoroutine(TypeoutDiscrip(sentance, SelectedText));
        }
        else
        {
            StartCoroutine(endSelfTalkDialogDiscrip());
        }
    }

    IEnumerator endSelfTalkDialogTitle()
    {
        yield return new WaitForSeconds(2.5f);

        INcroTitle = 0;
        TextoutTile = "";
    }

    IEnumerator endSelfTalkDialogDiscrip()
    {
        yield return new WaitForSeconds(2.5f);

        INcroDiscription = 0;
        TextoutDiscription = "";
    }


}
