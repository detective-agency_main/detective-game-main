using UnityEngine;

public class TriggerHiddenObjects : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    FindAndDisappear FindAndDisappearscript;
    Selected_HiddenObjectData[] selected_HiddenObjectDatast;

    void Start()
    {
        FindAndDisappearscript = FindFirstObjectByType<FindAndDisappear>();
        selected_HiddenObjectDatast = GetComponentsInChildren<Selected_HiddenObjectData>();
    }

    Main_playercontrol main_Playercontrol;
    bool WalkActive = true;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player") && WalkActive) 
        { 
           main_Playercontrol = other.gameObject.GetComponent<Main_playercontrol>();
           main_Playercontrol.SetStayMovement(false);
           main_Playercontrol.SetHiddenObjectsMenu();
           FindAndDisappearscript.Selectedparent(this.gameObject);
           FindAndDisappearscript.CollectChildrenData(selected_HiddenObjectDatast);

            WalkActive = false;
        }

    }

}
