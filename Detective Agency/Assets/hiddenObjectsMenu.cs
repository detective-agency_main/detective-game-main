using UnityEngine;
using UnityEngine.UI;

public class hiddenObjectsMenu : MonoBehaviour
{
    Sprite ObjectsNeededSprite;
    [SerializeField] Image ItemFindObjects;
    [SerializeField] Transform ContentSpawnpoint;
    Sort_HiddenObjects_Menu[] Sort_HiddenObjectsMenu;
    Main_playercontrol _Playercontrol;
    Canvas_Charater_Interaction canvas_Controller;
    Selected_HiddenObjectData[] hiddenObjectDatas;
    Object_InventoryImport Object_;

    // Start is called once before the first execution of Update after the MonoBehaviour is created
    void Start()
    {
        _Playercontrol = FindFirstObjectByType<Main_playercontrol>();
        canvas_Controller = FindFirstObjectByType<Canvas_Charater_Interaction>();
    }

    bool AddOnce = true;

    public void SetResetHiddenObjects()
    {
        AddOnce = true;
    }
    public void AddSelectedImportObjects(Selected_HiddenObjectData[] objectDatas)
    {
        if (AddOnce)
        {

            hiddenObjectDatas = objectDatas;

            foreach (var obj in hiddenObjectDatas) 
            {
                Object_ = obj.InventoryImport();
                ObjectsNeededSprite = Object_.ObjectInventory_Image;

                ItemFindObjects.sprite = ObjectsNeededSprite;
                Sort_HiddenObjects_Menu hiddenObjectinfo = ItemFindObjects.GetComponent<Sort_HiddenObjects_Menu>();
                hiddenObjectinfo.setData(Object_);

                Instantiate(ItemFindObjects, ContentSpawnpoint.transform);
            }
            AddOnce = false;
        }
    }
    public void SortSelectedHiddenData(Object_InventoryImport import)
    {
        Sort_HiddenObjectsMenu = FindObjectsByType<Sort_HiddenObjects_Menu>(FindObjectsInactive.Include, FindObjectsSortMode.None);

        foreach (var item in Sort_HiddenObjectsMenu)
        {
            if (item.inventoryImport_hiddenObjData.ObjectDataName == import.ObjectDataName)
            {
                item.DestroyHiddenObjectcollected(); // change image
            }
        }
    }

    public void ExitHiddenObjectsScreen()
    {
        canvas_Controller.SetHiddenObjectGame(false);
    }
}
