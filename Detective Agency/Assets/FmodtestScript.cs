using FMODUnity;
using System.Runtime.InteropServices;
using System;
using UnityEngine;
using FMOD.Studio;

public class FmodtestScript : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    AudioManager audioManager;
   // [SerializeField] EventReference SelectedAudio;

    
    [SerializeField] public EventReference fmodEvent;

    private FMOD.DSP fftDsp;
    private float[] spectrumData = new float[512]; // Array for storing spectrum data
    private FMOD.DSP mFFT;
    private float[] mFFTSpectrum;
    const int WindowSize = 1024;
    FMOD.Studio.EventInstance musicInstance;
    FMOD.DSP fft;

    [SerializeField] LineRenderer lineRenderer;

    void Start()
    {
        audioManager = gameObject.GetComponent<AudioManager>();

        lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer.positionCount = 1024;
        lineRenderer.SetWidth(.1f, .1f);

        musicInstance = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);

        FMODUnity.RuntimeManager.CoreSystem.createDSPByType(FMOD.DSP_TYPE.FFT, out fft);

        fft.setParameterInt((int)FMOD.DSP_FFT.WINDOWTYPE, (int)FMOD.DSP_FFT_WINDOW.HANNING);
        fft.setParameterInt((int)FMOD.DSP_FFT.WINDOWSIZE, WindowSize * 2);

        FMOD.ChannelGroup channelGroup;
        FMODUnity.RuntimeManager.CoreSystem.getMasterChannelGroup(out channelGroup);
        channelGroup.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD, fft);

        musicInstance.start();
    }
    const float WIDTH = 10.0f;
    const float HEIGHT = 0.1f;

    void Update()
    {
        IntPtr unmanagedData;
        uint length;
        fft.getParameterData((int)FMOD.DSP_FFT.SPECTRUMDATA, out unmanagedData, out length);
        FMOD.DSP_PARAMETER_FFT fftData = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(unmanagedData, typeof(FMOD.DSP_PARAMETER_FFT));
        var spectrum = fftData.spectrum;

        Debug.Log(fftData.numchannels);
        Debug.Log(fftData.numchannels > 0);
        if (fftData.numchannels > 0)
        {
            var pos = Vector3.zero;
            pos.x = WIDTH * -0.5f;

            for (int i = 0; i < WindowSize; ++i)
            {
                pos.x += (WIDTH / WindowSize);

                float level = lin2dB(spectrum[0][i]);
                pos.y = (80 + level) * HEIGHT;

                lineRenderer.SetPosition(i, pos);

                Debug.Log(pos);
            }
        }
    }

    float lin2dB(float linear)
    {
        return Mathf.Clamp(Mathf.Log10(linear) * 20.0f, -80.0f, 0.0f);
    }
    void OnDestroy()
    {
        // Stop and release the event instance when the script is destroyed
        /*eventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        eventInstance.release();*/
    }

    public void StopAudio()
    {
        //audioManager.StopAudio();
    }

    public void PlayAudio()
    {
      //  audioManager.DialogAudioPlay(SelectedAudio);
    }
}

    
