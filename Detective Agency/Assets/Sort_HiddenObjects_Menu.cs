using UnityEngine;

public class Sort_HiddenObjects_Menu : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    public Object_InventoryImport inventoryImport_hiddenObjData;
    

    public void setData(Object_InventoryImport import)
    {
        inventoryImport_hiddenObjData = import;
    }

    public void DestroyHiddenObjectcollected()
    {
        Destroy(this.gameObject);
    }
}
