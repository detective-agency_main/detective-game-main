using UnityEngine;

public class TimeLine_garage_scrpt : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    [SerializeField] GameObject[] Objectsactivate;
    [SerializeField] GameObject[] ObjectsDeactivate;
    public void SetMainGame()
    {
        foreach (GameObject obj in Objectsactivate)
        {
            obj.SetActive(true);
        }
        foreach (GameObject obj in ObjectsDeactivate)
        {
            obj.SetActive(false);
        }

    }
}
