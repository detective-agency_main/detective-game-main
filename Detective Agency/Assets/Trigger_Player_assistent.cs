using FMOD.Studio;
using FMODUnity;
using UnityEngine;

public class Trigger_Player_assistent : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created

    public enum WalkingState
    {
        PotrolHints,
        Stay,
        followDetective
    }

    [SerializeField] public int SelectedArray = 0;

    [SerializeField] public WalkingState walkingState;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player_assist")
        {
            other.GetComponent<petcontroller>().player_Assistent = this;
            other.GetComponent<petcontroller>().SetWalkingState(walkingState);
            other.GetComponent<petcontroller>().setSelected_ArrayPoints(SelectedArray); // hint 
        }
        else if (other.gameObject.tag == "Player")
        {
            other.GetComponent<Main_playercontrol>().SetAssistantCall();
        }
    }

}
