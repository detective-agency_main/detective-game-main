using UnityEngine;
using UnityEngine.UI;
public class VolumeControl : MonoBehaviour
{

    private FMOD.Studio.VCA vca_controller_Master;
    private FMOD.Studio.VCA vca_controller_Music;
    private FMOD.Studio.VCA vca_controller_SFX;
    private FMOD.Studio.VCA vca_controller_Voices;

    [Header("Sliders")]
    [SerializeField] Slider slider_Master;
    [SerializeField] Slider slider_Music;
    [SerializeField] Slider slider_SFX;
    [SerializeField] Slider slider_Voices;

    [Header("VCA Names")]
    public string VCA_name_master;
    public string VCA_name_Music;
    public string VCA_name_SFX;
    public string VCA_name_Voices;

    public float Value_master;
    public float Value_Music;
    public float Value_SFX;
    public float Value_Voices;
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    void Start()
    {
        vca_controller_Master = FMODUnity.RuntimeManager.GetVCA("VCA:/" + VCA_name_master);
        vca_controller_Music = FMODUnity.RuntimeManager.GetVCA("VCA:/" + VCA_name_Music);
        vca_controller_SFX = FMODUnity.RuntimeManager.GetVCA("VCA:/" + VCA_name_SFX);
        vca_controller_Voices = FMODUnity.RuntimeManager.GetVCA("VCA:/" + VCA_name_Voices);
        SetVolumes();
    }

    private void SetVolumes()
    {
        Value_master = PlayerPrefs.GetFloat("MasterVolume");
        Value_Music = PlayerPrefs.GetFloat("MusicVolume");
        Value_SFX = PlayerPrefs.GetFloat("SFXVolume");
        Value_Voices = PlayerPrefs.GetFloat("VoiceVolume");

        vca_controller_Master.setVolume(Value_master);
        vca_controller_Music.setVolume(Value_Music);
        vca_controller_SFX.setVolume(Value_SFX);
        vca_controller_Voices.setVolume(Value_Voices);

        slider_Master.value = Value_master;
        slider_Music.value = Value_Music;
        slider_SFX.value = Value_SFX;
        slider_Voices.value = Value_Voices;
    }

    public void MasterVCAController(float SelectedVCa)
    {
        Value_master = SelectedVCa;
        Debug.Log(Value_master);
        vca_controller_Master.setVolume(Value_master);
        PlayerPrefs.SetFloat("MasterVolume",Value_master);
    }

    public void MusicVCAController(float SelectedVCa)
    {
        Value_Music = SelectedVCa;
        vca_controller_Music.setVolume(Value_Music);
        PlayerPrefs.SetFloat("MusicVolume", Value_master);
    }

    public void SFXVCAController(float SelectedVCa)
    {
        Value_SFX = SelectedVCa;
        vca_controller_SFX.setVolume(Value_SFX);
        PlayerPrefs.SetFloat("SFXVolume", Value_master);
    }

    public void VoicesVCAController(float SelectedVCa)
    {
        Value_Voices = SelectedVCa;
        vca_controller_Voices.setVolume(Value_Voices);
        PlayerPrefs.SetFloat("VoiceVolume", Value_master);
    }
}
