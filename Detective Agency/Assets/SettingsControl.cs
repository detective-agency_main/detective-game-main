using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class SettingsControl : MonoBehaviour
{
    UniversalRenderPipelineAsset[] pipelineAsset;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool ChangeQuality = true;
    private void graphics_Control(bool Btn_type)
    {
        ChangeQuality = Btn_type;

        if (ChangeQuality)
        {

        }
        else
        {

        }
    }

    public void Front_Btn_Graphics_Change()
    {
        ChangeQuality = true;
        graphics_Control(ChangeQuality);
    }

    public void Back_Btn_Graphics_Change()
    {
        ChangeQuality = false;
        graphics_Control(ChangeQuality);
    }


    public void setQuailtyLevel(int level)
    {
        QualitySettings.SetQualityLevel(level);
    }
}
