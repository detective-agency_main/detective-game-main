using UnityEngine;

public class Selected_HiddenObjectData : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    [SerializeField] Object_InventoryImport ObjectDataImport;
    

    public Object_InventoryImport InventoryImport()
    {
        return ObjectDataImport;
    }
}
