using UnityEngine;

public class SortEvidence_dataList : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    [SerializeField] Hor_EvidenceList[] Hor_EvidenceGame_Content; // Hor_evidenceList is only used to find the objects to put it in, due to unable to find tag with in active
    [SerializeField] GameObject Hor_EvidenceGame_Content_Org;
    [SerializeField] GameObject Evidence_ItemInset;
    EvidenceItemManager EvidenceItemManager;
    private Object_InventoryImport importedEvidenceData;
    int loopevidenceCount = 0;

    public void SortLoaded_LoadIntoEvidence(Object_InventoryImport import)
    {
        importedEvidenceData = import;
        loopevidenceCount = 0;

        Hor_EvidenceGame_Content = FindObjectsByType<Hor_EvidenceList>(FindObjectsInactive.Include, FindObjectsSortMode.None);

        for (int i = 0; i < Hor_EvidenceGame_Content.Length; i++)
        {

            if (Hor_EvidenceGame_Content[i].transform.childCount == 4)
            {

                loopevidenceCount++;
                Debug.Log(loopevidenceCount + "loop count if child count of hor >");
                if (loopevidenceCount >= Hor_EvidenceGame_Content.Length)
                {
                    Instantiate(Hor_EvidenceGame_Content_Org, Hor_EvidenceGame_Content[i].transform);
                    loopevidenceCount = 0;
                    SortLoaded_LoadIntoEvidence(importedEvidenceData);

                }
                else
                {
                    Debug.Log("continue");
                    continue;
                }
            }
            else
            {
                EvidenceItemManager = Evidence_ItemInset.GetComponent<EvidenceItemManager>();
                EvidenceItemManager.setEvidenceInformation(importedEvidenceData);

                Instantiate(Evidence_ItemInset, Hor_EvidenceGame_Content[i].transform);
                return;

            }

        }
    }
}
