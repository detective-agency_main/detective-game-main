using FMOD.Studio;
using FMODUnity;
using UnityEngine;

public class TriggerNarativeTalk : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created

    Main_playercontrol playercontrol;
    [SerializeField] EventReference eventInstance;
    [SerializeField][TextArea] string Name = "neh";

    private void Start()
    {
        playercontrol = FindFirstObjectByType<Main_playercontrol>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<Main_playercontrol>().SetAssistantCall();
            playercontrol.Set_SelfTalk_Action(eventInstance, Name);
        }
    }

}
