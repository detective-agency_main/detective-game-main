using DG.Tweening.Core.Easing;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class TriggerInteraction_Dialog_select : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    GameManager gameManager;
    AudioManager audioManager;
    Main_playercontrol _Playercontrol_selfTalk;
    GameObject SelectedObject;
    triggerObjectInfoDialog ObjectDialogData;
    Canvas_Charater_Interaction mainCanvas;
    [SerializeField] private bool OnceClick = false;
    void Start()
    {
        _Playercontrol_selfTalk = FindAnyObjectByType<Main_playercontrol>();
        gameManager = FindAnyObjectByType<GameManager>();
        audioManager = FindAnyObjectByType<AudioManager>();
        mainCanvas = FindAnyObjectByType<Canvas_Charater_Interaction>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 MousePostion = Mouse.current.position.value;
        Ray ray = gameManager._camera.ScreenPointToRay(MousePostion);
        RaycastHit hit;

        if (Mouse.current.leftButton.isPressed)
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag == "interactableObject" && !OnceClick)
                {
                    OnceClick = true;

                    SelectedObject = hit.transform.gameObject;

                    ObjectDialogData = SelectedObject.GetComponent<triggerObjectInfoDialog>();

                    int rangeSelected = Random.Range(0,ObjectDialogData.SelectedAudio.Length);

                    _Playercontrol_selfTalk.Set_SelfTalk_Action(ObjectDialogData.SelectedAudio[rangeSelected], ObjectDialogData.SelectedAudio_Text[rangeSelected]);

                    StartCoroutine(ChangeBoolean());
                }
            }
        }
    }

    IEnumerator ChangeBoolean()
    {
        yield return new WaitForSeconds(1);
        OnceClick = false;
    }

}
