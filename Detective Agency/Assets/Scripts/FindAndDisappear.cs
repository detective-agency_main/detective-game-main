using DG.Tweening.Core.Easing;
using System.Collections;
using UnityEngine;

public class FindAndDisappear : MonoBehaviour
{
    // List of objects that must be found
    public GameObject[] objectsToFind;

    [SerializeField] Canvas_Charater_Interaction Canvas_Charater;
    [SerializeField] GameObject[] Parent_GameObjects;
    GameObject Selected_Parent;
    Object_InventoryImport SelectedObejct_ImportData;
    Main_playercontrol _Playercontrol;
    GameManager gameManager;
    triggerQuestCheck triggerQuestCheck;
    Selected_HiddenObjectData selectedObject_data;

    Selected_HiddenObjectData[] selected_HiddenObjectDatast;

    void Start()
    {
        // Initialize the total number of ob/*jects
        gameManager = FindAnyObjectByType<GameManager>();
        triggerQuestCheck = GetComponent<triggerQuestCheck>();
        _Playercontrol = FindAnyObjectByType<Main_playercontrol>();


    }
    // Update is called once per frame
    public void Selectedparent(GameObject triggerObject)
    {
        Selected_Parent = triggerObject;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                
                if (hit.transform.parent.gameObject == Selected_Parent)
                {
                    selectedObject_data = hit.collider.gameObject.GetComponent<Selected_HiddenObjectData>();
                    SelectedObejct_ImportData = selectedObject_data.InventoryImport();
                    SelectedObejct_ImportData.ObjectCollected = true;

                    if (SelectedObejct_ImportData.Evidence)
                    {
                        SelectedObejct_ImportData.ObjectFound = true;
                        Canvas_Charater.LoadIntoEvidence(SelectedObejct_ImportData);
                        Canvas_Charater.SortFoundHiddenObjects(SelectedObejct_ImportData);

                        StartCoroutine(ItemKillYourself(hit.collider.gameObject));

                        StartCoroutine(CheckSelectGameState());
                    }
                    else
                    {
                        Canvas_Charater.LoadIntoInventory(SelectedObejct_ImportData);

                        ItemKillYourself(hit.collider.gameObject);

                        StartCoroutine(CheckSelectGameState());
                        //  _Playercontrol.Set_SelfTalk_Action(InventoryItems_scriptables[i].Selected_Audio, InventoryItems_scriptables[i].Audio_Text);

                    }

                }

            }
        }
    }

    int GameObjectsLeftCount = 0;
    IEnumerator ItemKillYourself(GameObject Object)
    {
        Destroy(Object);

        yield return new WaitForSeconds(0.5f);
    }
    IEnumerator CheckSelectGameState()
    {
        yield return new WaitForSeconds(0.7f);

        if (Selected_Parent.transform.childCount == 0) // check child count of selected 
        {
            _Playercontrol.SetStayMovement(true);
            Canvas_Charater.SetHiddenObjectGame(false);
        }

        GameObjectsLeftCount = 0;
        for (int i = 0; i < objectsToFind.Length; i++)
        {
            if (objectsToFind[i] == null)
            {
                GameObjectsLeftCount++;

                if (GameObjectsLeftCount == objectsToFind.Length)
                {
                    triggerQuestCheck.TriggerQuestInfoChange();
                }
            }
        }
    }

    public void CollectChildrenData(Selected_HiddenObjectData[] objectDatas)
    {
        selected_HiddenObjectDatast = objectDatas;
       
        Canvas_Charater.SetDisplayObject(selected_HiddenObjectDatast);
    }

   }
