using UnityEngine;

public class HoverColorChange : MonoBehaviour
{
    private Color originalColor;  // Stores the original color of the object
    public Color hoverColor = Color.red;  // Color when hovered over
    private Renderer objectRenderer;

    // Start is called before the first frame update
    void Start()
    {

        // Get the Renderer component of the object
        objectRenderer = GetComponent<Renderer>();

        // Save the object's original color
        if (objectRenderer != null)
        {
            //originalColor = objectRenderer.material.color;
        }
    }

    // OnMouseEnter is called when the mouse enters the object's collider
    void OnMouseEnter()
    {
        // Change the object's color to hoverColor
        if (objectRenderer != null)
        {
            objectRenderer.material.color = hoverColor;
        }
    }

    // OnMouseExit is called when the mouse exits the object's collider
    void OnMouseExit()
    {
        // Revert the object's color back to its original color
        if (objectRenderer != null)
        {
            objectRenderer.material.color = originalColor;
        }
    }
}