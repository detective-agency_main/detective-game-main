using UnityEngine;
using UnityEngine.UI;

public class SketchEffect : MonoBehaviour
{
    public Image image;
    public float threshold = 0.5f;

    void OnEnable()
    {
        ApplySketchEffect();
    }

    void ApplySketchEffect()
    {
        if (image == null || image.sprite == null || image.sprite.texture == null)
        {
            Debug.LogError("Image or texture is missing!");
            return;
        }

        Texture2D originalTexture = image.sprite.texture;
        Texture2D sketchTexture = new Texture2D(originalTexture.width, originalTexture.height);

        Color[] pixels = originalTexture.GetPixels();
        Color[] newPixels = new Color[pixels.Length];

        for (int i = 0; i < pixels.Length; i++)
        {
            Color pixel = pixels[i];
            float gray = pixel.grayscale;
            float sketchValue = gray > threshold ? 1.0f : 0.0f;
            newPixels[i] = new Color(sketchValue, sketchValue, sketchValue, pixel.a);
        }

        sketchTexture.SetPixels(newPixels);
        sketchTexture.Apply();

        image.sprite = Sprite.Create(sketchTexture, new Rect(0, 0, sketchTexture.width, sketchTexture.height), new Vector2(0.5f, 0.5f));
    }
}