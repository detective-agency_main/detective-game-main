using UnityEngine;
using UnityEngine.UI;

public class ObjectSwapper : MonoBehaviour
{
    public GameObject object1;
    public GameObject object2;
    public Button swapButton;

    private bool isObject1Active = true;

    void Start()
    {
        // Initialize the objects' states
        object1.SetActive(true);
        object2.SetActive(false);

        // Add a click event listener to the button
        swapButton.onClick.AddListener(SwapObjects);
    }

    void SwapObjects()
    {
        // Toggle the active states of the objects
        isObject1Active = !isObject1Active;

        object1.SetActive(isObject1Active);
        object2.SetActive(!isObject1Active);
    }
}