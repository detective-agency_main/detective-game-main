using DG.Tweening.Core.Easing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class CameraControl : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject[] cameras;
    //[SerializeField] GameObject[] triggers; //make sure names of camera and trigger are the same
    public GameObject activecam;
    GameManager gamemanager;
    int previousselection = 0;
    int CamCount = 0;
    int CamLengh = 0;

    private void Awake()
    {
        gamemanager = FindAnyObjectByType<GameManager>();

        foreach (var item in cameras)
        {
            if (item.gameObject.activeInHierarchy)
            {
                activecam = item;
                return;
            }
        }

        CamLengh = cameras.Length;
    }

    private void Start()
    {
        gamemanager = FindAnyObjectByType<GameManager>();

        foreach (var item in cameras)
        {
            if (item.gameObject.activeInHierarchy)
            {
                activecam = item;
                return;
            }
        }

        CamLengh = cameras.Length;
    }
    public void setActiveCamera(GameObject ActivateCamera) 
    {
        
        ActivateCamera.SetActive(true);

        try
        {
            activecam.SetActive(false);
        }
        catch (System.Exception)
        {
            Debug.Log("no active Camera");
        }

        setNewActiveCamera();
    }

    private void setNewActiveCamera()
    {
        foreach (var SelCameras in cameras)
        {
            if (SelCameras.gameObject.activeInHierarchy)
            {
                activecam = SelCameras;
                return;
            }
        }
    }

    public void SetBacktoMainCamera(GameObject ActiveCamera_Exit)
    {
        ActiveCamera_Exit.SetActive(true);
        activecam.SetActive(false);
        setNewActiveCamera();
    }

    
    public void NextCamera()
    {

        if (CamCount >= cameras.Length - 1)
        {
            CamCount = cameras.Length - 1;
        }
        else
        {
            cameras[CamCount].SetActive(false);

            CamCount++;

            cameras[CamCount].SetActive(true);

            setNewActiveCamera();
        }

       
        
    }

    public void PreviousCamera()
    {
        if (CamCount <= 0)
        {
            CamCount = 0;
        }
        else
        {
            cameras[CamCount].SetActive(false);

            CamCount--;

            cameras[CamCount].SetActive(true);
            setNewActiveCamera();
        }
        
    }

}
