using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RotatePage : MonoBehaviour
{

    [SerializeField] RectTransform CoverPage;
    [SerializeField] Canvas CoverPageCanvas;

    [SerializeField] GameObject Book;


    [SerializeField] RectTransform PausePage;
    [SerializeField] RectTransform PauseSticky;
    [SerializeField] Canvas PauseStickyCanvas;
    [SerializeField] Vector3 RotationAmount;
    [SerializeField] float RotationSpeed;
    [SerializeField] GameObject PausePageContent;


    [SerializeField] RectTransform SettingsPage;
    [SerializeField] RectTransform SettingsSticky;
    [SerializeField] Canvas SettingsStickyCanvas;
    [SerializeField] GameObject SettingsPageContent;

    [SerializeField] RectTransform HowToPlayPage;
    [SerializeField] RectTransform HowToPlaySticky;
    [SerializeField] Canvas HowToPlayStickyCanvas;
    [SerializeField] GameObject HowToPlayPageContent;

    [SerializeField] GameObject MainMenuPageContent;
    GameManager manager;


    private void Start()
    {
        manager = FindFirstObjectByType<GameManager>();
    }

    private void Update()
    {
        if (CoverPage.eulerAngles.y >= 90f)
        {
            CoverPageCanvas.sortingOrder = 1;
        }
        if (CoverPage.eulerAngles.y <= 90f)
        {
            CoverPageCanvas.sortingOrder = 7;
        }
        if (PausePage.eulerAngles.y >= 90f)
        {
            PauseSticky.localEulerAngles = new Vector3(0,180,0);
            PauseStickyCanvas.sortingOrder = 2;
            PausePageContent.SetActive(false);
        }
        if (PausePage.eulerAngles.y <= 90f)
        {
            PausePage.GetComponent<Canvas>().sortingOrder = 6;
        }
        if (SettingsPage.eulerAngles.y >= 90f)
        {
            SettingsSticky.localEulerAngles = new Vector3(0, 180, 0);
            SettingsStickyCanvas.sortingOrder = 2;
            SettingsPageContent.SetActive(false);
        }
        if (SettingsPage.eulerAngles.y <= 90f)
        {
            SettingsPageContent.SetActive(true);
            SettingsPage.GetComponent<Canvas>().sortingOrder = 5;
        }
        if (HowToPlayPage.eulerAngles.y >= 90f)
        {
            HowToPlaySticky.localEulerAngles = new Vector3(0, 180, 0);
            HowToPlayStickyCanvas.sortingOrder = 2;
            HowToPlayPageContent.SetActive(false);
        }
        if (HowToPlayPage.eulerAngles.y <= 90f)
        {
            HowToPlayPageContent.gameObject.SetActive(true);
            HowToPlayPage.GetComponent<Canvas>().sortingOrder = 4;
        }
        
    }
    public void RotateToSettings()
    {

        PausePage.DORotate(RotationAmount,RotationSpeed);

        if (SettingsPage.eulerAngles.y >= 90f)
        {
            SettingsPage.DORotate(Vector3.zero,RotationSpeed);
           
        }
      
    }

    public void RotateBookCover()
    {

        CoverPage.DORotate(RotationAmount, RotationSpeed);

    }
    public void RotateToHowToPlay()
    {

        SettingsPage.DORotate(RotationAmount, RotationSpeed);
        PausePage.DORotate(RotationAmount, RotationSpeed);
        if (HowToPlayPage.eulerAngles.y >= 90f)
        {
            HowToPlayPage.DORotate(Vector3.zero, RotationSpeed);
           
        }
    }

    public void RotateToMainMenu()
    {
        HowToPlayPage.DORotate(RotationAmount, RotationSpeed);
        SettingsPage.DORotate(RotationAmount, RotationSpeed);
        PausePage.DORotate(RotationAmount, RotationSpeed);
    }

    public void CloseBook()
    {
        
        CoverPage.DORotate(Vector3.zero, RotationSpeed).OnComplete(() =>
        {
            Book.SetActive(false);
            MainMenuPageContent.SetActive(false);
        });

        PausePage.DORotate(Vector3.zero, RotationSpeed * 0.9f);
        SettingsPage.DORotate(Vector3.zero, RotationSpeed * 0.8f);
        HowToPlayPage.DORotate(Vector3.zero, RotationSpeed * 0.7f);
       
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(sceneBuildIndex:0);
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    public void Quite()
    {
        Application.Quit();
    }

    public void SaveGameData()
    {
       
    }

}
