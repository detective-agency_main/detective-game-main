using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page : MonoBehaviour
{
    private bool pageTurning;
    public bool pageOpened;
    private bool updatedSortingOrder;
    public RectTransform StickyNoteButton;


    private void Update()
    {
        if (pageTurning && !updatedSortingOrder)
        {
            if (transform.eulerAngles.y >= 90f && !pageOpened)
            {
                GetComponent<Canvas>().sortingOrder += 1;
                updatedSortingOrder = true;
            }
            else if (transform.eulerAngles.y < 90 && pageOpened)
            {
                GetComponent<Canvas>().sortingOrder -= 1;
                updatedSortingOrder = true;
            }
        }
    }

    public void RotatePageLeft()
    {
        pageTurning = true;

        transform.DORotate(PauseMenu.instance.RotationAmount, PauseMenu.instance.RotationTime).OnComplete(() => { pageTurning = false
            ;
            pageOpened = true; 
            updatedSortingOrder = false; 
            if(StickyNoteButton != null) StickyNoteButton.localEulerAngles = new Vector3(0, 180, 0);});
 
    }

    public void RotatePageRight()
    {
        pageTurning = true;
        transform.DORotate(Vector3.zero, PauseMenu.instance.RotationTime).OnComplete(() => { pageTurning = false
            ;
            pageOpened = false; 
            updatedSortingOrder = false; 
            if (StickyNoteButton != null) StickyNoteButton.localEulerAngles = new Vector3(0, 0, 0); });
    }
}
