using UnityEngine;

public class UIMenuManager : MonoBehaviour
{
    public GameObject journalStart;
    public GameObject journalSuspects;
    public GameObject journalCasebook;
    public GameObject journalEvidence;
    public GameObject journalNotes;
    public GameObject journalMap;

    private const string lastMenuKey = "LastMenu";

    // Start is called before the first frame update
    void Start()
    {
        ShowJournalStart();
    }

    public void ShowJournalStart()
    {
        SetAllMenusInactive();
        journalStart.SetActive(true);
    }

    public void ShowJournalSuspects()
    {
        SetAllMenusInactive();
        journalSuspects.SetActive(true);
        SaveLastMenu("JournalSuspects");
    }

    public void ShowJournalCasebook()
    {
        SetAllMenusInactive();
        journalCasebook.SetActive(true);
        SaveLastMenu("JournalCasebook");
    }

    public void ShowJournalEvidence()
    {
        SetAllMenusInactive();
        journalEvidence.SetActive(true);
        SaveLastMenu("JournalEvidence");
    }

    public void ShowJournalNotes()
    {
        SetAllMenusInactive();
        journalNotes.SetActive(true);
        SaveLastMenu("JournalNotes");
    }

    public void ShowJournalMap()
    {
        SetAllMenusInactive();
        journalMap.SetActive(true);
        SaveLastMenu("JournalMap");
    }

    private void SetAllMenusInactive()
    {
        journalStart.SetActive(false);
        journalSuspects.SetActive(false);
        journalCasebook.SetActive(false);
        journalEvidence.SetActive(false);
        journalNotes.SetActive(false);
        journalMap.SetActive(false);
    }

    private void SaveLastMenu(string menuName)
    {
        PlayerPrefs.SetString(lastMenuKey, menuName);
        PlayerPrefs.Save();
    }

    public void ShowLastMenu()
    {
        string lastMenu = PlayerPrefs.GetString(lastMenuKey, "JournalSuspects");
        switch (lastMenu)
        {
            case "JournalSuspects":
                ShowJournalSuspects();
                break;
            case "JournalCasebook":
                ShowJournalCasebook();
                break;
            case "JournalEvidence":
                ShowJournalEvidence();
                break;
            case "JournalNotes":
                ShowJournalNotes();
                break;
            case "JournalMap":
                ShowJournalMap();
                break;
            default:
                ShowJournalSuspects();
                break;
        }
    }
}