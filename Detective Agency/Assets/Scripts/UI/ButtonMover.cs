using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class ButtonSwitcher : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public RectTransform button1Rect; // Reference to the first button's RectTransform
    public RectTransform button2Rect; // Reference to the second button's RectTransform
    public float duration = 0.5f; // Duration of the movement
    public float scaleDuration = 0.2f; // Duration of the scaling
    public Vector3 hoverScale = new Vector3(0.79493f, 0.79493f, 0.79493f); // Scale when hovered

    private Vector3 originalScale = Vector3.one; // Original scale of the buttons

    public void SwapPositions()
    {
        // Swap positions of button1 and button2
        Vector3 tempPosition = button1Rect.localPosition;

        button1Rect.DOLocalMove(button2Rect.localPosition, duration);
        button2Rect.DOLocalMove(tempPosition, duration);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        RectTransform buttonRect = eventData.pointerEnter.GetComponent<RectTransform>();
        buttonRect.DOScale(hoverScale, scaleDuration);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        RectTransform buttonRect = eventData.pointerEnter.GetComponent<RectTransform>();
        buttonRect.DOScale(originalScale, scaleDuration);
    }
}