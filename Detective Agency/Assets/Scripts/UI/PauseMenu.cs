using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu instance;
    public GameObject book;
    public Canvas BookHolder;
    private bool pageTurning;
    public Vector3 RotationAmount;
    public float RotationTime;
    public List<Page> pages = new List<Page>();


    private void Awake()
    {
        instance = this;
    }

    public void TurnTOActivePage(Page IncomingPage)
    {
        TurnSelectedPage(IncomingPage);
        foreach (Page page in pages)
        {
             
            if (page != IncomingPage && !page.pageOpened)
            {
                page.RotatePageLeft();
            }
            else if (page != IncomingPage && page.pageOpened)
            {
                page.RotatePageRight();
                Debug.Log("turn selected page");
            }
            else
            {
                break;
            }
        }
    }

    public void OpenFile(Page IncomingPage)
    {
        IncomingPage.RotatePageLeft();
    }

    public void TurnSelectedPage(Page IncomingPage)
    {
        Debug.Log("turn page clicked");
        if (IncomingPage.pageOpened == true)
        {
            IncomingPage.RotatePageRight();
        }
        else
        {
            IncomingPage.RotatePageLeft();
        }
    }

}
