using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using DG.Tweening;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ScaleOnHover : MonoBehaviour
{
    [SerializeField] GameObject Inventory;

    [SerializeField] Vector3 ScaleValue;
    [SerializeField] float ScaleDuration;
    [SerializeField] Transform ItemToDrag;
    [SerializeField] bool iconIsBeingDragged;
    [SerializeField] GameObject Draging;
    [SerializeField] GameObject RaycastBox;
    [SerializeField] Image InventoryImage;

    [SerializeField] DetectiveAgency PlayerInputs;

    private void Awake()
    {
        PlayerInputs = new DetectiveAgency();
        PlayerInputs.Enable();
    }

    private void Update()
    {
       
        if (iconIsBeingDragged)
        {
            PointerDrag();
        }
    }

    public void LeftClick()
    {
       // iconIsBeingDragged = true;
    
    }

    //ScaleUIBigger
    public void PointerEnter()
    {
        transform.DOScale(ScaleValue,ScaleDuration);
    }
    //ScaleUIToNormal
    public void PointerExit()
    {
        transform.DOScale(1,ScaleDuration);
    }

    //DragUI
    public void PointerDrag()
    {
        ItemToDrag.transform.SetParent(Draging.transform);
        ItemToDrag.position = PlayerInputs.UI.Point.ReadValue<Vector2>();
        RaycastBox.SetActive(true);
    }

    public void ExitInventory()
    {
        if (iconIsBeingDragged)
        {
            Inventory.SetActive(false);
        }
    }

    public void setInventoryInformation(Object_InventoryImport importedObjectData)
    {
        InventoryImage.sprite = importedObjectData.ObjectInventory_Image;
    }
}
