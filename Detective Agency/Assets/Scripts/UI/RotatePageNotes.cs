using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class RotatePageNotes : MonoBehaviour, IPointerClickHandler, IPointerDownHandler,IPointerUpHandler
{
    public GameObject textObject; // Reference to the text GameObject
    public float rotationSpeed = 50f; // Rotation speed

    public bool isRotating = false; // Flag to track if the object is currently rotating
    public bool isRotated = false;  // Flag to track if the object has rotated

    // Called when the object is clicked
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log ($"{gameObject}");
        if (!isRotating)
        {
            // Determine the target rotation angle based on the current state
            float targetAngle = isRotated ? -180f : 180f;
            
            // Rotate the GameObject to the target angle using DoTween
            RotateTo(targetAngle);
        }
    }

     public void OnPointerDown(PointerEventData eventData)
    {}
     public void OnPointerUp(PointerEventData eventData)
    {}

    // Method to rotate the GameObject to a specific angle using DoTween
    private void RotateTo(float angle)
    {
        isRotating = true;

        // Deactivate the text GameObject during rotation
        textObject.SetActive(false);
       Debug.Log ($"Angle is {angle} and {gameObject}");
       transform.DORotate(new Vector3(angle, 0f, 0f), 0f, RotateMode.LocalAxisAdd);
        transform.DORotate(new Vector3(0f, angle, 0f), 1f, RotateMode.LocalAxisAdd)
                 .SetEase(Ease.Linear)
                 .OnComplete(() =>
                 {
                     // Rotation completed
                     
                     isRotating = false; // Reset rotation flag
                     isRotated = !isRotated; // Toggle rotation state

                     // Reactivate the text GameObject after rotation is completed
                      if(!isRotated) 
                      {
                        textObject.SetActive(true);
                      }
                 });
    }
}