using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ImageUIController : MonoBehaviour
{
    public Image displayImage;  
    public Image[] images;       
    public Button nextButton;    
    public Button backButton;    
    public Button exitButton;    

    private int currentImageIndex = 0; 
    private string sceneKey;            

    void Start()
    {
        sceneKey = "Scene_" + SceneManager.GetActiveScene().name + "_HasSeenUI"; 

        exitButton.gameObject.SetActive(false); 
        backButton.gameObject.SetActive(false); 

        nextButton.onClick.AddListener(NextImage);
        backButton.onClick.AddListener(BackImage);
        exitButton.onClick.AddListener(ExitUI);

        if (PlayerPrefs.GetInt(sceneKey, 0) == 1)
        {
            gameObject.SetActive(false); 
            HideCursor();                
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            ShowCurrentImage(); 
            ShowCursor();       
        }
    }

    private void ShowCurrentImage()
    {
        if (images.Length > 0 && currentImageIndex < images.Length)
        {
            displayImage.sprite = images[currentImageIndex].sprite; 
            displayImage.gameObject.SetActive(true);                
        }
    }

    public void NextImage() 
    {
        if (currentImageIndex < images.Length - 1)
        {
            currentImageIndex++;
            UpdateUI(); 
        }
    }

    public void BackImage() 
    {
        if (currentImageIndex > 0)
        {
            currentImageIndex--;
            UpdateUI(); 
        }
    }

    private void UpdateUI()
    {
        ShowCurrentImage(); 
        backButton.gameObject.SetActive(currentImageIndex > 0); 
        nextButton.gameObject.SetActive(currentImageIndex < images.Length - 1); 
        exitButton.gameObject.SetActive(currentImageIndex >= images.Length - 1); 
    }

    public void ExitUI() 
    {
        PlayerPrefs.SetInt(sceneKey, 1); 
        gameObject.SetActive(false);       
        HideCursor();                     
    }

    private void OnDestroy()
    {
        PlayerPrefs.SetInt(sceneKey, 0); 
    }

    private void ShowCursor()
    {
        Cursor.visible = true;                  
        Cursor.lockState = CursorLockMode.None; 
    }

    private void HideCursor()
    {
        Cursor.visible = false;                  
        Cursor.lockState = CursorLockMode.Locked; 
    }
}