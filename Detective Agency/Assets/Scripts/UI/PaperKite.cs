using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PaperAirplaneAnimation : MonoBehaviour
{
    public RectTransform airplaneImage;
    public RectTransform targetUIObject;
    public Vector2 endPosition;
    public float flightDuration = 2f;
    public CanvasGroup targetCanvasGroup;
    public float flippedScaleX = -1f;
    public float returnZRotationAngle = 360f;

    private Vector2 originalPosition;
    private Vector3 originalScale;
    private float originalZRotation;

    private void Start()
    {
        originalPosition = airplaneImage.anchoredPosition;
        originalScale = airplaneImage.localScale;
        originalZRotation = airplaneImage.eulerAngles.z;
    }

    public void OnFlyButtonPressed()
    {
        StartFlight();
    }

    private void StartFlight()
    {
        Sequence flightSequence = DOTween.Sequence();

        flightSequence
            .Append(airplaneImage.DOAnchorPos(endPosition, flightDuration)
                .SetEase(Ease.InOutQuad))
            .OnComplete(() =>
            {
                airplaneImage.DOScaleX(flippedScaleX, 0.5f)
                    .OnComplete(TransitionToUI);
            });
    }

    private void TransitionToUI()
    {
        if (targetCanvasGroup != null)
        {
            targetCanvasGroup.DOFade(1, 1f);
        }
    }

    public void OnBackButtonPressed()
    {
        airplaneImage.gameObject.SetActive(true);

        Sequence backSequence = DOTween.Sequence();

        backSequence
            .Append(airplaneImage.DOAnchorPos(originalPosition, flightDuration)
                .SetEase(Ease.InOutQuad))
            .Join(airplaneImage.DORotate(new Vector3(0, 0, originalZRotation + returnZRotationAngle), flightDuration))
            .OnComplete(() =>
            {
                airplaneImage.DOScaleX(originalScale.x, 0.5f)
                    .OnComplete(ResetUI);
            });
    }

    private void ResetUI()
    {
        if (targetCanvasGroup != null)
        {
            targetCanvasGroup.DOFade(0, 1f);
        }

        airplaneImage.rotation = Quaternion.Euler(0, 0, originalZRotation);
    }
}