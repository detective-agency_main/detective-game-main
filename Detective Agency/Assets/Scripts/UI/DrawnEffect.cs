using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DrawingAnimation : MonoBehaviour
{
    public Image imageToDraw; // Reference to the image being drawn in
    public float animationDuration = 2f; // Duration of the drawing animation
    public float endAlpha = 1f; // Final alpha value of the image

    void Start()
    {
        // Start animation to draw in the image
        imageToDraw.DOFade(endAlpha, animationDuration);
    }
}