using System.Collections.Generic;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public List<GameObject> cameras; // List of cameras to switch between

    private int currentCameraIndex = 0;

    private void Start()
    {
        // Ensure only the first camera is enabled at the start
        for (int i = 0; i < cameras.Count; i++)
        {
            cameras[i].SetActive(i == currentCameraIndex);
        }
    }

    public void SwitchCamera()
    {
        // Turn off the current camera
        cameras[currentCameraIndex].SetActive(false);

        // Move to the next camera index (looping back to the start if necessary)
        currentCameraIndex++;
        Debug.Log(currentCameraIndex);
        // Turn on the next camera
        cameras[currentCameraIndex].SetActive(true);
    }
}
