using UnityEngine;
using UnityEngine.EventSystems; // Required when using Event data.
using DG.Tweening;

public class ImageHoverEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public float hoverMoveDistance = 30f; // Distance to move the image to the right
    public float duration = 0.5f; // Duration of the movement

    private Vector3 originalPosition; // Store the original position of the image

    void Start()
    {
        // Store the original position of the image
        originalPosition = transform.localPosition;
    }

    // Do this when the pointer enters the object
    public void OnPointerEnter(PointerEventData eventData)
    {
        // Move the image to the right
        transform.DOLocalMoveX(originalPosition.x + hoverMoveDistance, duration);
    }

    // Do this when the pointer exits the object
    public void OnPointerExit(PointerEventData eventData)
    {
        // Return the image to its original position
        transform.DOLocalMove(originalPosition, duration);
    }
}