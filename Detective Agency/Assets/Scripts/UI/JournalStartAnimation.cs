using UnityEngine;
using DG.Tweening;

public class JournalStartAnimation : MonoBehaviour
{
    public RectTransform journalStartRectTransform;
    public float animationDuration = 1f;
    public float startYPosition = -1000f; // Adjust this value to be below the visible area
    public float endYPosition = 0f;

    void Start()
    {
        // Set the initial position of the menu below the visible area
        journalStartRectTransform.anchoredPosition = new Vector2(journalStartRectTransform.anchoredPosition.x, startYPosition);

        // Move the menu to the end position with animation
        journalStartRectTransform.DOAnchorPosY(endYPosition, animationDuration)
            .SetEase(Ease.OutQuad); // You can adjust the ease type as needed (e.g., Ease.OutQuad)
    }
}