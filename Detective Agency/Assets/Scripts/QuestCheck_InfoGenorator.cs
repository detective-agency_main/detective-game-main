using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestCheck_InfoGenorator : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    [SerializeField] public Quest_Information_Holder QuestInfo_holder;
    [SerializeField] TextMeshProUGUI MainQuestName;
    [SerializeField] TextMeshProUGUI MainQuestDiscription;
    [SerializeField] Image MainQuestStatusImage;
    [SerializeField] Image ItemFindObjects;
    [SerializeField] public bool QuestStatus = false;
    [SerializeField] GameObject ContentSpawnpoint;

    GameManager gameManager;

    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();

        MainQuestName.text = QuestInfo_holder.QuestName;
        MainQuestDiscription.text = QuestInfo_holder.QuestDescription;
        CheckQuestItemInfo();
        // add image as well
    }

    // Update is called once per frame
    void Update()
    {
        if (QuestStatus)
        {
            if (QuestInfo_holder.QuestComplete)
            {
                QuestStatus = false;
                gameManager.ChangeQuestListHolder(QuestInfo_holder.name);
                Destroy(this.gameObject);
            }
        }
    }

    public void CheckInfoStatus()
    {
        QuestStatus = true;
    }

    FindAndDisappear Disappearscript;
    Object_InventoryImport[] importObjects;
    Sprite ObjectsNeeded;
    public void CheckQuestItemInfo() 
    {
        if (QuestInfo_holder.findObjects)
        {
            Disappearscript = FindFirstObjectByType<FindAndDisappear>();

           // importObjects = Disappearscript.InventoryItems_scriptables;

            //for (int i = 0; i < importObjects.Length; i++)
            //{
            //    ObjectsNeeded = importObjects[i].ObjectInventory_Image;

            //    ItemFindObjects.sprite = ObjectsNeeded;

            //    Instantiate(ItemFindObjects, ContentSpawnpoint.transform);
            //}
            
        }
    }
}
