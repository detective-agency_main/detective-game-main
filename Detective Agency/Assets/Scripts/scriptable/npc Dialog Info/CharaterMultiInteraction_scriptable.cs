using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Multi_Charater_Interaction",menuName = "Multi_Charater_InteractionData")]
public class CharaterMultiInteraction_scriptable : ScriptableObject
{

    [SerializeField] public string charaterName;
    [SerializeField] public int Cameratype;
    [SerializeField] public Sprite charaterSprite;

    [Header("Player Interaction Questions")]

    [SerializeField] [TextArea] public List<string> PlayerQuestions = new List<string>();

    [SerializeField] public List<bool> PlayerQuestions_stupidMeter = new List<bool>();
    [SerializeField] public List<bool> PlayerQuestions_CleverMeter = new List<bool>();

    [SerializeField] public List<EventReference> PlayerAudio = new List<EventReference>();


    [Header("Other Character Interaction Questions")]

    [SerializeField] [TextArea] public List<string> NPCQuestions = new List<string>();
    [SerializeField] public List<EventReference> NPCAudio = new List<EventReference>();


    [Header("anded conversation")]
    string ConversationNameSet;

    [ContextMenu("mehh")]
    private void blaa()
    {

    }
   
}
