using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest_Information_Holder", menuName = "Scriptable Objects/Quest_Information_Holder")]
public class Quest_Information_Holder : ScriptableObject
{
    [SerializeField] public bool DialogOrFind;
    [SerializeField] public bool findObjects;

    [SerializeField] public string QuestName;

    [TextArea]
    [SerializeField] public string QuestDescription;

    [SerializeField] public bool QuestComplete;


    private void Awake()
    {
      QuestComplete = false;
    }

    void Start()
    {
        QuestComplete = false;
    }
}
