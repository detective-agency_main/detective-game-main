using FMODUnity;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialog_InteractionHolder", menuName = "Scriptable Objects/Dialog_InteractionHolder")]
public class Dialog_InteractionHolder : ScriptableObject
{
    [TextArea]
    [SerializeField] public string[] SelectedAudio_Text;
    [SerializeField] public EventReference[] SelectedAudioList;

}
