using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Inventory_ListData",menuName = "Inventory Object Data")]
public class Object_InventoryImport : ScriptableObject
{
    [SerializeField] public bool Evidence;
    [SerializeField] public bool ObjectFound = false;
    [SerializeField] public string ObjectDataName;
    [SerializeField] public string Object_Clue;
    [SerializeField] public Sprite ObjectInventory_Image;
    [SerializeField] public EventReference Selected_Audio;
    [SerializeField] [TextArea] public string Audio_Text;

    [SerializeField] public bool ObjectCollected = false;

}
