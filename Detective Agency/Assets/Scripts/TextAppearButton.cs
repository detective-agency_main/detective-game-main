using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonClickHandler : MonoBehaviour
{
    public TextMeshProUGUI[] textToShowArray; // Array of TextMeshPro components to show/hide
    public Button[] buttons; // Array of Button components
    public GameObject mainObjective;
     public GameObject sideObjective;

    private TextMeshProUGUI activeText; // Reference to the currently active TextMeshPro object

    void Start()
    {
        // Add listeners to the button click events
        for (int i = 0; i < buttons.Length; i++)
        {
            int index = i; // Store current index in a local variable to avoid closure issues
            buttons[i].onClick.AddListener(() => OnButtonClick(index));
        }
    }

    // Method to handle button click event
    void OnButtonClick(int buttonIndex)
    {
        // Deactivate the currently active text, if any
        // DeactivateActiveText();

        // Activate the TextMeshPro object associated with the clicked button
        // activeText = textToShowArray[buttonIndex];
        // activeText.gameObject.SetActive(true);
        ObjectiveDisplay.instance.mainObjectiveActive = !ObjectiveDisplay.instance.mainObjectiveActive;
        mainObjective.SetActive(ObjectiveDisplay.instance.mainObjectiveActive);
        sideObjective.SetActive(!ObjectiveDisplay.instance.mainObjectiveActive);
      
    }

    // Method to deactivate the currently active text
    void DeactivateActiveText()
    {
        if (activeText != null)
        {
            activeText.gameObject.SetActive(false);
        }
    }
}