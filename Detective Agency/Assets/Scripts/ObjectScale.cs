using UnityEngine;

public class HoverScale : MonoBehaviour
{
    private Vector3 originalScale;    // Stores the original scale of the object
    public float scaleFactor = 1.1f;  // Determines how much bigger the object gets on hover

    void Start()
    {
        // Save the object's original scale
        originalScale = transform.localScale;
    }

    void OnMouseEnter()
    {
        // Scale the object up by the scaleFactor
        transform.localScale = originalScale * scaleFactor;
    }

    void OnMouseExit()
    {
        // Return the object to its original scale
        transform.localScale = originalScale;
    }
}