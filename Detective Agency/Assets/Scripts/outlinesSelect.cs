using DG.Tweening.Core.Easing;
using UnityEngine;
using UnityEngine.InputSystem;

public class outlinesSelect : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    GameManager gameManager;
    [SerializeField] Material outlines;
    private string SelectedTag = "Untagged";
    private string PreSelectedTag = "Untagged";
    private GameObject Highlite;
    private GameObject currentObject;

    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
    }

    bool OnceOff = true;
    // Update is called once per frame
    void Update()
    {

        Vector2 MousePostion = Mouse.current.position.value;
        Ray ray = gameManager._camera.ScreenPointToRay(MousePostion);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {

            if (hit.transform.gameObject.tag != "Duplicated")
            {
                SelectedTag = hit.collider.gameObject.tag;
            }

            if (hit.collider.gameObject.tag == "interactableObject" || hit.collider.gameObject.tag == "FindObjects")
            {

                var selection = hit.transform.gameObject;
                currentObject = selection.gameObject;

                if (OnceOff)
                {
                   OnceOff = false;
                   var Gameobjectme =  Instantiate(selection, selection.transform.position, selection.transform.rotation, selection.transform);
                   Highlite = Gameobjectme;

                    Vector3 Changescale = new Vector3(1f,1f,1f);
                    Gameobjectme.tag = "Duplicated";
                    Gameobjectme.GetComponent<MeshCollider>().enabled = false;
                    Gameobjectme.transform.localScale = Changescale;

                   MeshRenderer Meshrendererere = Gameobjectme.GetComponent<MeshRenderer>();
                   Meshrendererere.material = outlines;
                }
            }

                if (SelectedTag != PreSelectedTag)
                {
                    PreSelectedTag = SelectedTag;
                    OnceOff = true; 
                    Destroy(Highlite);
                }
        }


    }
}
