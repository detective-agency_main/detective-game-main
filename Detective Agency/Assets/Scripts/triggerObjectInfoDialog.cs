using DG.Tweening.Core.Easing;
using FMODUnity;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class triggerObjectInfoDialog : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    GameManager gameManager;
    
    [SerializeField] private Dialog_InteractionHolder dataHolder;
    public EventReference[] SelectedAudio;
    public string[] SelectedAudio_Text;
    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        SelectedAudio = dataHolder.SelectedAudioList;
        SelectedAudio_Text = dataHolder.SelectedAudio_Text;
    }


}
