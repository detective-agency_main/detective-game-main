using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class EvidenceItemManager : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    [SerializeField] public Object_InventoryImport EvidenceImport_objectData;
    [SerializeField] Image InventoryImage;
    SelectedEvidenceData evidenceData;

    public void setEvidenceInformation(Object_InventoryImport objectData)
    {
        Debug.Log(objectData.ObjectDataName);
        EvidenceImport_objectData = objectData;
        InventoryImage.sprite = EvidenceImport_objectData.ObjectInventory_Image;
    }

    public void SetSelectedDataDisplay()
    {
        evidenceData = FindFirstObjectByType<SelectedEvidenceData>();
        evidenceData.SetInformation(EvidenceImport_objectData);
    }
}
