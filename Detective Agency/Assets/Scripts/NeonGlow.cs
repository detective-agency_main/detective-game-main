using UnityEngine;
using UnityEngine.UI;

public class NeonGlow : MonoBehaviour
{
    public Image mainImage; // The main neon image
    public Image glowImage; // The glow image behind the main image
    public Color neonColor = Color.cyan; // Neon color for the main image
    public Color glowColor = new Color(0, 1, 1, 0.5f); // Glow color, adjustable alpha

    private void Start()
    {
        // Apply colors to the images
        ApplyNeonGlow();
    }

    private void ApplyNeonGlow()
    {
        // Set the color of the main image to the neon color
        mainImage.color = neonColor;

        // Set the color of the glow image to the glow color
        glowImage.color = glowColor;
    }
}