using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction_Holder : MonoBehaviour
{
    [Header("Set Button Or Straight Dialog")]
    [SerializeField] public bool SetMultiDialog;
    [Header("Character Dialog Interactions")]
    [SerializeField] public CharaterMultiInteraction_scriptable[] charaterInteract_Multi;
    [Header("Quest Information")]
    [SerializeField] public Quest_Information_Holder Quest_Information;
    [SerializeField] public GameObject ClerkImage;
    [HideInInspector]  Vector3 LocationSet;
    [Header("Character Move Adjustment")]
    [SerializeField] public Vector3 LocationAdjust;
    [Header("Spoke to Character Audio")]
    [SerializeField] public EventReference[] PassthroughAudio;
    [SerializeField] public GameObject PopUpmessage;

    [Header("Evidence To Give")]
    [SerializeField] public bool EvidenceToGive = false;
    [SerializeField] public Object_InventoryImport evidenceData;

    [Header("Back Image Overlay")]
    [SerializeField] public Sprite BackgroundImage;
    private void Start()
    {
        LocationSet = this.transform.position;
        LocationSet += LocationAdjust;
    }
}
