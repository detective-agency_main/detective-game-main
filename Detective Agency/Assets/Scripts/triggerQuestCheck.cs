using UnityEngine;

public class triggerQuestCheck : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    [SerializeField] Quest_Information_Holder Questinfo;
    GameManager gameManager;

    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TriggerQuestInfoChange()
    {
        gameManager.ChangeQuestInfo(Questinfo.QuestName);
    }
}
