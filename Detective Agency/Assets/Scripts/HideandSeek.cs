using UnityEngine;

public class TextDisappearance : MonoBehaviour
{
    // Reference to the text game object
    public GameObject textObject;

    void Update()
    {
        // Check for mouse click
        if (Input.GetMouseButtonDown(0))
        {
            // Cast a ray from the mouse position into the scene
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // Check if the ray hits any object
            if (Physics.Raycast(ray, out hit))
            {
                // Check if the hit object is the one you want to trigger text disappearance
                if (hit.collider.gameObject != gameObject)
                {
                    // If the hit object is not the text object, disable it
                    textObject.SetActive(false);
                }
            }
        }
    }
}