using UnityEngine;
using TMPro;
using System.Collections;

public class TypewriterEffectTMP : MonoBehaviour
{
    public TMP_Text tmpText; // Reference to the TextMeshPro Text component
    public float typingSpeed = 0.05f; // Delay between each character

    private string fullText; // The full text to display
    private string currentText = ""; // The text currently displayed

    void Awake()
    {
        // Ensure the TextMeshPro Text component is set and get the full text
        if (tmpText == null)
        {
            tmpText = GetComponent<TMP_Text>();
        }

        fullText = tmpText.text;
        tmpText.text = "";

        // Start the typewriter effect
        StartCoroutine(TypeText());
    }

    IEnumerator TypeText()
    {
        foreach (char letter in fullText.ToCharArray())
        {
            currentText += letter;
            tmpText.text = currentText;
            yield return new WaitForSeconds(typingSpeed);
        }
    }
}