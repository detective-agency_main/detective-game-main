﻿using System.Collections.Generic;
using UnityEngine;

public static class SaveDataManager
{
    public static void SaveJsonData(IEnumerable<ISaveable> a_Saveables)
    {
        DetectiveSaveData sd = new DetectiveSaveData();
        foreach (var saveable in a_Saveables)
        {
            saveable.PopulateSaveData(sd);
        }

        if (FileManager.WriteToFile("SaveData01.dat", sd.ToJason()))
        {
            Debug.Log("Save successful");
        }
    }
    
    public static void LoadJsonData(IEnumerable<ISaveable> a_Saveables)
    {
        if (FileManager.LoadFromFile("SaveData01.dat", out var json))
        {
            DetectiveSaveData sd = new DetectiveSaveData();
            sd.LoadFromJason(json);

            foreach (var saveable in a_Saveables)
            {
                saveable.LoadFromSaveData(sd);
            }
            
            Debug.Log("Load complete");
        }
    }
}
