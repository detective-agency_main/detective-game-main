using UnityEngine;
using System.Collections.Generic;
using System;

[System.Serializable]
public class DetectiveSaveData
{

    public int LoadScene;

    public string ToJason()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJason(string L_Jason) 
    {
        JsonUtility.FromJsonOverwrite(L_Jason, this);
    }


    public void SaveSelectedData()
    {

    }

}

public interface ISaveable
{
    void PopulateSaveData(DetectiveSaveData saveData);
    void LoadFromSaveData(DetectiveSaveData saveData);
}
