using DG.Tweening;
using FMOD.Studio;
using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class Canvas_Charater_Interaction : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Main Player")]
    [SerializeField] Main_playercontrol MainPlayer;

    [Header("Text Objects")]
    [SerializeField] TextMeshProUGUI txt_Name;
    [SerializeField] TextMeshProUGUI txt_Conversation;
    [SerializeField] TextMeshProUGUI txt_SlefTalk;

    [Header("Image Objects")]
    [SerializeField] UnityEngine.UI.Image SpriteCharacter;
    [SerializeField] Sprite SpritePlayer;

    [Header("Content Spawns Objects")]
    [SerializeField] GameObject QuestInfoObjectSpawner;
    [SerializeField] GameObject QuestInfoObjectSpawnerItemsFind;
    [SerializeField] GameObject QuestInfoObjectSpawnerItemsFindCase;
    [SerializeField] GameObject QuestInfoObjectSpawnerCase;
    [SerializeField] GameObject contentlistLocation;
    [SerializeField] GameObject contentlistLocationQuests;
    [SerializeField] GameObject contentlistLocationCase;

    [Header("Buttons Objects")]
    [SerializeField] GameObject BtnConversationMulti;

    [Header("game Managements")]
    [SerializeField] GameManager MainManager;
    [SerializeField] AudioManager AudioManagerMain;

    [Header("gameDialog Managements")]
    [SerializeField] GameObject MultiScrollSection;
    [SerializeField] GameObject NormalDialogSection;
    [SerializeField] GameObject MainImagesBackground;
    [SerializeField] GameObject MainInteractionCanvasDIa;

    [Header("Inventory and Evedience Managements")]
    [SerializeField] GameObject InventoryGame_Content;
    [SerializeField] GameObject Inventory_ItemInset;

    [SerializeField] GameObject Ver_EvidenceGame_Content;
    [SerializeField] SortEvidence_dataList LoadEvidenceScript;

    [Header("Boxes Managements")]
    [SerializeField] GameObject SelfTalkDialog;
    [SerializeField] GameObject QuestCompletePage;
    [SerializeField] GameObject QuestBar;

    [SerializeField] GameObject HiddenObjectMenu;
    [SerializeField] hiddenObjectsMenu Sort_HiddenObjects;
     
    [HideInInspector] public CharaterMultiInteraction_scriptable SelectedCharaterMulti;
    [HideInInspector] public Object_InventoryImport SelectedObjectData;
    bool MultiConvosation = false;

    btn_alterInfo btn_;
    int lineCountMaxDw = 0;


    bool CheckTypeOutEnd = true;

    int INcro = 1;
    string Textout = "";

    int lineLimite = 0;

    string playersentance;
    string NPCsentance;

    int conversationccountNPC;
    int convNPCLiveCount = -1; // set if it should be 0 or -1

    int conversationccountPlayer;
    int convPlayerLiveCount = 0; //same here

    int conversationccount = 0;

    //int convNPCLiveCountMulti = -1;
    //int convPlayerLiveCountMulti = 0;
    int ConvoCountMovement = 0;

    int conversationccountNPCMulti = 0;
    int conversationccountPlayerMulti = 0;

    [SerializeField] UnityEngine.UI.Image Stupid_Meter;
    [SerializeField] UnityEngine.UI.Image Clever_Meter;


    TextMeshProUGUI proUGUI;

    int LineLimite_Multi = 0;

    enum CurrentActor
    {
        Player,
        NPC
    }

    CurrentActor Actor = CurrentActor.Player;

    bool BStupid_Meter = false;
    bool BClever_Meter = false;

    private void Start()
    {
        MainManager = FindAnyObjectByType<GameManager>();
        SpriteCharacter.color = new Color(SpriteCharacter.color.r, SpriteCharacter.color.g, SpriteCharacter.color.b, 0f);

        AudioManagerMain = GetComponent<AudioManager>();

        NormalDialogSection.SetActive(false);
        MultiScrollSection.SetActive(false);
        SelfTalkDialog.SetActive(false);
        MainImagesBackground.SetActive(false);

        SetQuestInformationSpawn();
        StartCoroutine(SetHideQuestBar());
    }

    int QuestCount = 0;
    QuestCheck_InfoGenorator QuestGenorator;

    IEnumerator SetHideQuestBar()
    {
        yield return new WaitForSeconds(4f);
        QuestBar.transform.DOMoveY(1300f,3f);
    }

    public void showQuestBar()
    {
        QuestBar.transform.DOMoveY(900f, 3f);
    }

    public void HideQuestBar()
    {
        QuestBar.transform.DOMoveY(1270f, 3f);

    }
    private void SetQuestInformationSpawn()
    {
        QuestCount = MainManager.SceneQuestInformation.Length;

        for (int i = 0; i < QuestCount; i++)
        {

            if (MainManager.SceneQuestInformation[i].findObjects)
            {
                QuestGenorator = QuestInfoObjectSpawnerItemsFind.GetComponent<QuestCheck_InfoGenorator>();
                QuestGenorator.QuestInfo_holder = MainManager.SceneQuestInformation[i];

                Instantiate(QuestInfoObjectSpawnerItemsFind, contentlistLocationQuests.transform);

                QuestGenorator = QuestInfoObjectSpawnerItemsFindCase.GetComponent<QuestCheck_InfoGenorator>();
                QuestGenorator.QuestInfo_holder = MainManager.SceneQuestInformation[i];

                Instantiate(QuestInfoObjectSpawnerItemsFindCase, contentlistLocationCase.transform);

            }
            else
            {
                QuestGenorator = QuestInfoObjectSpawner.GetComponent<QuestCheck_InfoGenorator>();
                QuestGenorator.QuestInfo_holder = MainManager.SceneQuestInformation[i];

                Instantiate(QuestInfoObjectSpawner, contentlistLocationQuests.transform);

                QuestGenorator = QuestInfoObjectSpawnerCase.GetComponent<QuestCheck_InfoGenorator>();
                QuestGenorator.QuestInfo_holder = MainManager.SceneQuestInformation[i];

                Instantiate(QuestInfoObjectSpawnerCase, contentlistLocationCase.transform);

            }


        }
    }
    Quest_Information_Holder SelectedCharacterInformation;
    Interaction_Holder SelectedInteraction;
    [SerializeField] UnityEngine.UI.Image BackgroundDialogImage;
    public void setCharaterDialog(CharaterMultiInteraction_scriptable charaterInteract_Multi,Quest_Information_Holder _Information_Holder,Interaction_Holder interaction_Holder)
    {
        SelectedCharaterMulti = charaterInteract_Multi;
        SelectedCharacterInformation = _Information_Holder;
        SelectedInteraction = interaction_Holder;

        if (SelectedCharacterInformation.QuestComplete)
        {
            int ran = UnityEngine.Random.Range(0, SelectedInteraction.PassthroughAudio.Length);
            AudioManagerMain.DialogAudioPlay(SelectedInteraction.PassthroughAudio[ran]);
        }
        else
        {
            SetNameText();
        }
    }

    public void SetDialogType(bool Type)
    {
        MultiConvosation = Type;
    }
    private void SetNameText()
    {
        SpriteCharacter.color = new Color(SpriteCharacter.color.r, SpriteCharacter.color.g, SpriteCharacter.color.b, 1f);
        BackgroundDialogImage.GetComponent<UnityEngine.UI.Image>().sprite = SelectedInteraction.BackgroundImage;
        BackgroundDialogImage.sprite = SelectedInteraction.BackgroundImage;
        txt_Name.text = SelectedCharaterMulti.charaterName;

        if (!MultiConvosation)
        {
            SpriteCharacter.sprite = SelectedCharaterMulti.charaterSprite;
            Debug.Log("Playin23123g");
            MultiScrollSection.SetActive(false);
            NormalDialogSection.SetActive(true);
            MainImagesBackground.SetActive(true);
            Invoke("startConverstation", 1); // coinverstaion start count
        }
        else
        {
            SpriteCharacter.sprite = SelectedCharaterMulti.charaterSprite;

            NormalDialogSection.SetActive(true);
            MainImagesBackground.SetActive(true);
            Invoke("Multi_startConverstation", 1);  // coinverstaion start count

        }
    }

    public void SetCanvasInteract_Menu()
    {
        if (!MultiConvosation)
        {
            SpriteCharacter.sprite = SelectedCharaterMulti.charaterSprite;
            MultiScrollSection.SetActive(false);
            NormalDialogSection.SetActive(true);
            MainImagesBackground.SetActive(true);
            Invoke("startConverstation", 1); // coinverstaion start count
        }
        else
        {
            SpriteCharacter.sprite = SelectedCharaterMulti.charaterSprite;

            NormalDialogSection.SetActive(true);
            MainImagesBackground.SetActive(true);
            Invoke("Multi_startConverstation", 1);  // coinverstaion start count

        }
    }

    public void SortFoundHiddenObjects(Object_InventoryImport import)
    {
        Sort_HiddenObjects.SortSelectedHiddenData(import);
    }

    string spectrumstuff = "";
    private void Update()
    {
        //GetSpectrum_TextDelay();
    }

    public void SetHiddenObjectGame(bool state)
    {
        HiddenObjectMenu.SetActive(state);
    }

    int btnSelectcount = 0;
    public enum StupidMeter
    {
        Non,
        SP,
        CP
    }

    StupidMeter meterReader = StupidMeter.Non;
    StupidMeter PreDetermain_meterReader = StupidMeter.Non;

    EventInstance audio_del;
    public void BtnSrollClick(int btnSelect,StupidMeter meter)
    {
        if (AudioManagerMain.IsPlaying())
        {
            audio_del = AudioManagerMain.GetInstanceDialog();
            AudioManagerMain.StopAudio(audio_del);
            Actor = CurrentActor.Player;
        }

        ConvoCountMovement++;
        txt_Conversation.text = "";
        btnSelectcount = btnSelect;
        meterReader = meter;
        checkSmartMeter();
        CheckTypeOutEnd = true;
        Multi_NextLineConverstation(lineCountMaxDw, ConvoCountMovement);
    }

    // turn into one value!!!!!!!!!!!!!
    float SPMeterMovement = 0;
    float CPMeterMovement = 0;

    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    private void checkSmartMeter()
    {
        switch (meterReader)
        {
            case StupidMeter.SP:
                SPMeterMovement += 0.2f;
                Stupid_Meter.fillAmount += SPMeterMovement;
                Clever_Meter.fillAmount -= SPMeterMovement;
                break;
            case StupidMeter.CP:
                CPMeterMovement += 0.2f;
                Stupid_Meter.fillAmount -= CPMeterMovement;
                Clever_Meter.fillAmount += CPMeterMovement;
                break;
            case StupidMeter.Non:
                SPMeterMovement += (int)StupidMeter.Non;
                CPMeterMovement += (int)StupidMeter.Non;
                Stupid_Meter.fillAmount -= SPMeterMovement;
                Clever_Meter.fillAmount += CPMeterMovement;
                break;
         
        }

    }

    private void setConvLivecounts()
    {
        convNPCLiveCount = -1;
        convPlayerLiveCount = -1;
    }

    private int GetLimitValues(int NPC, int Player)
    {
        /// extra space for 
        /// charater has other  stuff to say

        int limite = 0;

        if (NPC > Player)
        {
            limite = NPC;
        }
        else
        {
            limite = Player;
        }

        return limite;
    }
    bool typeSentence = false;
    bool InConverstation = false;
    private void startConverstation()
    {
        InConverstation = true;
        setConvLivecounts();
        conversationccountNPC = SelectedCharaterMulti.NPCQuestions.Count;
        conversationccountPlayer = SelectedCharaterMulti.PlayerQuestions.Count;

        lineLimite = GetLimitValues(conversationccountNPC, conversationccountPlayer);

        // see who starts conversation
        NextLineConverstation(conversationccount);
    }
    
    private void NextLineConverstation(int currentline)
    {
        if (currentline < lineLimite)
        {

            if (convPlayerLiveCount == convNPCLiveCount)
            {
                conversationccount = currentline + 1;
            }

            switch (Actor)
            {
                case CurrentActor.Player:
                    convPlayerLiveCount++;
                    Debug.Log("player");
                    playersentance = SelectedCharaterMulti.PlayerQuestions[convPlayerLiveCount];
                    txt_Name.text = SetCharacterTalkingName(SelectedCharaterMulti.PlayerQuestions[convPlayerLiveCount]);
                    string[] PlayerSentanceSplit = playersentance.Split(' ');
                    StartCoroutine(Typeout(PlayerSentanceSplit));
                    AudioManagerMain.DialogAudioPlay(SelectedCharaterMulti.PlayerAudio[convPlayerLiveCount]);
                    break;
                case CurrentActor.NPC:
                    convNPCLiveCount++;
                    Debug.Log("npc");
                    NPCsentance = SelectedCharaterMulti.NPCQuestions[convNPCLiveCount];
                    txt_Name.text = SetCharacterTalkingName(SelectedCharaterMulti.NPCQuestions[convNPCLiveCount]);
                    string[] NPCSentanceSplit = NPCsentance.Split(' ');
                    StartCoroutine(Typeout(NPCSentanceSplit));
                    AudioManagerMain.DialogAudioPlay(SelectedCharaterMulti.NPCAudio[convNPCLiveCount]);
                    break;
            }

        }
        else
        {
            conversationccount = 0;
            StartCoroutine(SendLineEnd());
            EndConversation = true;
        }

    }

    private string SetCharacterTalkingName(string Lines)
    {
        string NameLine = "";

        for (int i = 0; i < Lines.Length; i++)
        {
            if (Lines[i].ToString() == ":")
            {
                break;
            }

            NameLine += Lines[i];
        }

        return NameLine;
    }
    public void CallNextLine()
    {
        // fix with new code completely
        if (InConverstation && !MultiConvosation)
        {
            conversationccount++;

            if (EndConversation)
            {
                if (MultiConvosation)
                {
                    StartCoroutine(SendMultiLineEnd());
                }
                else
                {
                    StartCoroutine(SendLineEnd());
                }
            }
            else
            {
                if (MultiConvosation)
                {
                    StartCoroutine(SendMultiLineConvo());
                }
                else
                {
                    txt_Conversation.text = "";

                    switch (Actor)
                    {
                        case CurrentActor.Player:
                            convPlayerLiveCount++;
                            playersentance = SelectedCharaterMulti.PlayerQuestions[convPlayerLiveCount];
                            txt_Name.text = SetCharacterTalkingName(SelectedCharaterMulti.PlayerQuestions[convPlayerLiveCount]);
                            string[] PlayerSentanceSplit = playersentance.Split(' ');
                            StartCoroutine(Typeout(PlayerSentanceSplit));
                            AudioManagerMain.DialogAudioPlay(SelectedCharaterMulti.PlayerAudio[convPlayerLiveCount]);
                            break;
                        case CurrentActor.NPC:
                            convNPCLiveCount++;
                            NPCsentance = SelectedCharaterMulti.NPCQuestions[convNPCLiveCount];
                            txt_Name.text = SetCharacterTalkingName(SelectedCharaterMulti.NPCQuestions[convNPCLiveCount]);
                            string[] NPCSentanceSplit = NPCsentance.Split(' ');
                            StartCoroutine(Typeout(NPCSentanceSplit));
                            AudioManagerMain.DialogAudioPlay(SelectedCharaterMulti.NPCAudio[convNPCLiveCount]);
                            break;
                    }

                    if (convPlayerLiveCount == convNPCLiveCount)
                    {
                        conversationccount++;
                    }
                }
            }
        }
    }

    /// multi converstation
    public int onconutnpc;
    private void Multi_startConverstation()
    {
        MultiScrollSection.SetActive(true);
        SelfTalkDialog.SetActive(false);
        conversationccountNPCMulti = SelectedCharaterMulti.NPCQuestions.Count;
        conversationccountPlayerMulti = SelectedCharaterMulti.PlayerQuestions.Count;
        onconutnpc = conversationccountNPCMulti;
        LineLimite_Multi = GetLimitValues(conversationccountNPCMulti, conversationccountPlayerMulti);
        
        // see who starts conversation
        SetButtons();
    }
    
    bool EndConversation = false;
    private void Multi_NextLineConverstation(int Linecount, int currentline)
    {
        if (currentline >= Linecount)
        {
            CheckTypeOutEnd = false;
            playersentance = SelectedCharaterMulti.PlayerQuestions[btnSelectcount];
            string[] PlayerSentanceSplitEnd = playersentance.Split(' ');
            AudioManagerMain.DialogAudioPlay(SelectedCharaterMulti.PlayerAudio[btnSelectcount]);
            StartCoroutine(Typeout(PlayerSentanceSplitEnd));
            EndConversation = true;
        }
        else
        {
            if (CheckTypeOutEnd)
            {
                switch (Actor)
                {
                    case CurrentActor.Player:

                        CheckTypeOutEnd = false;
                        playersentance = SelectedCharaterMulti.PlayerQuestions[btnSelectcount];
                        string[] PlayerSentanceSplit = playersentance.Split(' ');
                        AudioManagerMain.DialogAudioPlay(SelectedCharaterMulti.PlayerAudio[btnSelectcount]);
                        StartCoroutine(Typeout(PlayerSentanceSplit));

                        break;
                    case CurrentActor.NPC:

                        CheckTypeOutEnd = false;
                        NPCsentance = SelectedCharaterMulti.NPCQuestions[btnSelectcount];
                        string[] NPCSentanceSplit = NPCsentance.Split(' ');
                        AudioManagerMain.DialogAudioPlay(SelectedCharaterMulti.NPCAudio[btnSelectcount]);
                        StartCoroutine(Typeout(NPCSentanceSplit));

                        break;
                }
            }
        }
    }

    private void GetSpectrum_TextDelay()
    {
        Debug.Log(AudioManagerMain.GetSpectrumstuff());
    }

    string Btn_ButtonTypeOut = "";
    bool InfnoComeBack = false;
    private string ChangeButtonDisplay(string Sentence)
    {
        Btn_ButtonTypeOut = "";
        string[] SperatedWords = Sentence.Split(' ');

        int randomLineCount = UnityEngine.Random.Range(5, 8);

        for (int i = 0; i < randomLineCount; i++)
        {
            if (i >= SperatedWords.Length)
            {
                break;
            }

            Btn_ButtonTypeOut += SperatedWords[i] + " ";
        }

        return Btn_ButtonTypeOut;
    }

    IEnumerator Typeout(string[] sentance)
    {
       // GetSpectrum_TextDelay();

        yield return new WaitForSeconds(0.5f);

            int countLengh = sentance.Length;
            Textout += sentance[INcro] + " ";
            txt_Conversation.text = Textout;
            INcro++;

            if (INcro < countLengh)
            {
                StartCoroutine(Typeout(sentance));
            }
            else
            {
                if (Actor == CurrentActor.Player)
                {
                    Actor = CurrentActor.NPC;
                    CheckTypeOutEnd = true;
                }
                else if (Actor == CurrentActor.NPC)
                {
                    Actor = CurrentActor.Player;
                    CheckTypeOutEnd = false;
                }

                INcro = 1;
                Textout = "";

                if (EndConversation)
                {
                    if (MultiConvosation)
                    {
                        StartCoroutine(SendMultiLineEnd());
                    }
                    else
                    {
                        StartCoroutine(SendLineEnd());
                    }
                }
                else
                {
                    if (MultiConvosation)
                    {
                        StartCoroutine(SendMultiLineConvo());
                    }
                    else
                    {
                        StartCoroutine(SendLineConvo());
                    }
                }
            }
    }

    public void TriggerQuestsComplete_screen()
    {
        QuestCompletePage.SetActive(true);
        Invoke("triggerCloseQuestComplete_page",2);
    }
    public void triggerCloseQuestComplete_page()
    {
        QuestCompletePage.SetActive(false);
    }

    private void SetButtons()
    {
        for (int i = 0; i < conversationccountPlayerMulti; i++)
        {
            btn_ = BtnConversationMulti.GetComponent<btn_alterInfo>();
            btn_.m_TextMeshPro.text = ChangeButtonDisplay(SelectedCharaterMulti.PlayerQuestions[i]);


            btn_.Stupid_MeterValue = SelectedCharaterMulti.PlayerQuestions_stupidMeter[i];
            btn_.Clever_MeterValue = SelectedCharaterMulti.PlayerQuestions_CleverMeter[i];

            BStupid_Meter = SelectedCharaterMulti.PlayerQuestions_stupidMeter[i];
            BClever_Meter = SelectedCharaterMulti.PlayerQuestions_CleverMeter[i];
            checkStupidMeterValue();

            btn_.canvas_inter = this.gameObject.GetComponent<Canvas_Charater_Interaction>();
            btn_.SetValueLine = i;

            lineCountMaxDw = conversationccountPlayerMulti;
            Instantiate(BtnConversationMulti, contentlistLocation.transform);
        }
    }

    void checkStupidMeterValue()
    {
        if (!BStupid_Meter && !BClever_Meter)
        {
            PreDetermain_meterReader = StupidMeter.Non;
        }
        else if (BStupid_Meter)
        {
            PreDetermain_meterReader = StupidMeter.SP;
        }
        else
        {
            PreDetermain_meterReader = StupidMeter.CP;
        }
    }

    IEnumerator SendMultiLineConvo()
    {
        yield return new WaitForSeconds(1f);
        txt_Conversation.text = "";
        Multi_NextLineConverstation(lineCountMaxDw, ConvoCountMovement);
    }

    IEnumerator SendLineConvo()
    {
        yield return new WaitForSeconds(1f);
        txt_Conversation.text = "";
        NextLineConverstation(conversationccount);

    }
    IEnumerator SendMultiLineEnd()
    {
        yield return new WaitForSeconds(1f);
        Multi_stopConverstation();
        InConverstation = false;
        MainManager.ChangeQuestInfo(SelectedCharacterInformation.QuestName);
        CheckPlayerConversationChange(SelectedCharacterInformation.QuestName);
    }

    IEnumerator SendLineEnd()
    {
        yield return new WaitForSeconds(1f);
        stopConverstation();
        InConverstation = false;
        MainManager.ChangeQuestInfo(SelectedCharacterInformation.QuestName);
        CheckPlayerConversationChange(SelectedCharacterInformation.QuestName);
    }
    private void Multi_stopConverstation()
    {
        lineCountMaxDw = 0;
        ConvoCountMovement = 0;
        MainManager.setconversation = false;

        MainInteractionCanvasDIa.SetActive(false);
        SelfTalkDialog.SetActive(false);
        SelectedInteraction.PopUpmessage.SetActive(false);
        MainImagesBackground.SetActive(false);

        MainPlayer.SetMovementControl();
        MainPlayer.SetStayMovement(true);

        if (SelectedInteraction.evidenceData)
        {
            LoadIntoEvidence(SelectedInteraction.evidenceData);
        }

    }

    private void stopConverstation()
    {
        lineCountMaxDw = 0;
        ConvoCountMovement = 0;
        MainManager.setconversation = false;

        MainInteractionCanvasDIa.SetActive(false);
        SelfTalkDialog.SetActive(false);
        SelectedInteraction.PopUpmessage.SetActive(false);
        MainImagesBackground.SetActive(false);

        MainPlayer.SetMovementControl();
        MainPlayer.SetStayMovement(true);

        if (SelectedInteraction.evidenceData)
        {
            LoadIntoEvidence(SelectedInteraction.evidenceData);
        }

    }

    public void StopConversationCall()
    {
        if (MultiConvosation)
        {
            Multi_stopConverstation();
        }
        else
        {
            stopConverstation();
        }

    }
    public bool DisableClick_OnAudioCheck()
    {
        return false;
    }

    public void LoadIntoInventory(Object_InventoryImport imported)
    {
        SelectedObjectData = imported;

        Inventory_ItemInset.GetComponent<ScaleOnHover>().setInventoryInformation(SelectedObjectData);
        Instantiate(Inventory_ItemInset,InventoryGame_Content.transform);

    }
    public void LoadIntoEvidence(Object_InventoryImport imported)
    {
        SelectedObjectData = imported;
        LoadEvidenceScript.SortLoaded_LoadIntoEvidence(imported);

    }
    Selected_HiddenObjectData[] _HiddenObjectDatas;
    public void SetDisplayObject(Selected_HiddenObjectData[] selected_Hiddens)
    {
        _HiddenObjectDatas = selected_Hiddens;
        Sort_HiddenObjects.SetResetHiddenObjects();
        Sort_HiddenObjects.AddSelectedImportObjects(selected_Hiddens);
    }

    

    const String Victoria_Name = "victoria";
    private void CheckPlayerConversationChange(string NameOfNPC)
    {
        if (NameOfNPC == Victoria_Name)
        {
           MainPlayer.ChangeToNextConverstation();
        }
    }
}
