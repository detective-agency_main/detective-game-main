using FMODUnity;
using System;
using System.Collections;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Main_playercontrol : MonoBehaviour
{
    GameManager gameManager;
    NavMeshAgent agent;
    AudioManager audioManager;
    [HideInInspector] public Rigidbody PlayerRB;

    [SerializeField] CameraControl controlCam;

    [SerializeField] private EventReference PlayerBreath;
    [SerializeField] petcontroller liyaControl;
    [SerializeField] public GameObject detectiveImage;

    private Animator animator;

    Vector3 AgentLocation;
    Interaction_Holder InteractionInfo;
    [HideInInspector] public CharaterMultiInteraction_scriptable[] SelectedCharatermulti;

    [HideInInspector] public bool MultiDialog = false;
    [SerializeField] Canvas_Charater_Interaction canvasinteraction;
    [SerializeField] GameObject canvas_Display;

    bool setMovement = true;
    bool setMovementControl = false;
    bool MovementToTarget = false;
    bool setactiveMovement = true;
    bool setLocationpointbool = true;
    [SerializeField] GameObject locationPoint;


    [Header("selfTalk section")]
    [SerializeField] private EventReference[] PLayerTalksHimself;
    [SerializeField] private string[] PLayerTalksHimself_Text;
    [SerializeField] TextMeshProUGUI SelfTalkText;
    [SerializeField] GameObject SelfTalkTextImage;
    Rigidbody mainMovement;
    int SelectedConverstationScript = 0;

    private void Awake()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        audioManager = FindFirstObjectByType<AudioManager>();
        liyaControl = FindFirstObjectByType<petcontroller>();
    }

    void Start()
    {
        PlayerRB = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        agent.isStopped = false;
        animator = GetComponentInChildren<Animator>();
        AgentLocation = this.transform.position;
        SelfTalkTextImage.SetActive(false);
        var audio = PLayerTalksHimself[0];
        MovementToTarget = false;

        animator.SetBool("DetectiveIdle", true);

        //audioManager.PlayOneShot(PLayerTalksHimself[0],this.transform.position);
        //Set_SelfTalk_Action(0);

        if (SceneManager.GetActiveScene().name == "Manner")
        {
            Debug.Log("here");
        }
    }

    Vector3 SavedPos;
    Vector2 PrePosition_SavedPos;
    [SerializeField] private bool StayState = true;
    bool MoveTONPCState = true;


    public void ChangeToNextConverstation()
    {
        SelectedConverstationScript++;
    }
    public void CheckClickAction()
    {
        Vector2 MousePostion = Mouse.current.position.value;
        Ray ray = gameManager._camera.ScreenPointToRay(MousePostion);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.collider.gameObject.tag);
            SavedPos = hit.point;
            setMovementControl = true;  
            if (hit.collider.gameObject.tag == "ground")
            {
                locationPoint.SetActive(true);
                locationPoint.transform.position = hit.point;

                agent.ResetPath();
                MovementToTarget = false;
                agent.isStopped = false;
                agent.stoppingDistance = 0.0f;
                AgentLocation = hit.point;
                

                ClickToMovement();
            }
            else if (hit.collider.gameObject.tag == "Inter")
            {
                agent.ResetPath();
                InteractionInfo = hit.collider.GetComponent<Interaction_Holder>();
                agent.isStopped = false;
                MoveTONPCState = true;
                AgentLocation = hit.point + InteractionInfo.LocationAdjust;
                agent.destination = AgentLocation;
                locationPoint.SetActive(false);

                ClickSelection();
            }
        }
    }

    void Update()
    {
       movelocationPoint();

        CheckDistance(); // for animation only
        if (StayState)
        {

            if (MovementToTarget)
            { 
                CheckDistanceTotarget();
            }

            if (setMovementControl)
            {
                if (groundSelect)
                {
                    agent.destination = AgentLocation;
                }
                else
                {
                    if (MoveTONPCState)
                    {
                        agent.isStopped = false;
                        agent.destination = AgentLocation;
                        agent.stoppingDistance = 0f;

                        float newdistance = Vector2.Distance(this.transform.position, AgentLocation);
                        if (agent.remainingDistance < 4f || newdistance < 4f)
                        {
                            MoveTONPCState = false;
                            MovementToTarget = true;
                            setMovementControl = false;
                            setLocationpointbool = false;
                            agent.isStopped = true;
                        }
                        
                    }
                    else
                    {
                       agent.isStopped = true;
                    }

                }
            }
        }

        RotateImageDetective();
    }

    

    public void StopMovementState_Menu(bool state)
    {
        StayState = state;
        setactiveMovement = state;
        agent.isStopped = state;
    }

    int INcro = 0;
    string Textout = "";

    public void SetStayMovement(bool state)
    {
        StayState = state;
        //detectiveImage.SetActive(state);
        agent.isStopped = !state;
    }

    public void SetHiddenObjectsMenu()
    {
        canvasinteraction.SetHiddenObjectGame(true);
    }
    public void SetAssistantCall()
    {
        liyaControl.ChangetoMovestate();
    }
    private void CheckDistance() // for animation only
    {
        animator.SetBool("DetectiveIdle", true);
        animator.SetBool("DetectiveWalk", false);

        float newdistance = Vector2.Distance(this.transform.position, SavedPos);
        if (newdistance < 1f)
        {
                animator.SetBool("DetectiveIdle", true);
                animator.SetBool("DetectiveWalk", false);
        }
        else
        {
                animator.SetBool("DetectiveWalk", true);
                animator.SetBool("DetectiveIdle", false);
        }

    }

    IEnumerator Typeout(string sentance)
    {
        yield return new WaitForSeconds(0.08f); // how quick it types out

        int countLengh = sentance.Length;

        Textout += sentance[INcro];

        SelfTalkText.text = Textout;
        INcro++;

        if (INcro < countLengh)
        {
            StartCoroutine(Typeout(sentance));
        }
        else
        {
            StartCoroutine(endSelfTalkDialog());
        }
    }

    IEnumerator endSelfTalkDialog()
    {
        yield return new WaitForSeconds(2.5f);

        INcro = 0;
        Textout = "";
        SelfTalkText.text = Textout;
        SelfTalkTextImage.SetActive(false);
    }

    public void Set_SelfTalk_Action(EventReference selectedAudio,string AudioText)
    {
        //audioManager.StopAudio(selectedAudio);
        audioManager.PlayOneShot(selectedAudio,gameManager._camera.transform.position);
        SelfTalkTextImage.SetActive(true);
        StartCoroutine(Typeout(AudioText));
    }

    public void Dialog_SelfTalk_Action(int Selected)
    {
        //audioManager.StopAudio(PLayerTalksHimself[Selected]);
        audioManager.PlayOneShot(PLayerTalksHimself[Selected], this.transform.position);
        SelfTalkTextImage.SetActive(true);
        StartCoroutine(Typeout(PLayerTalksHimself_Text[Selected]));
    }


    private void movelocationPoint()
    {

        if (setLocationpointbool)
        {
            locationPoint.SetActive(true);
        }
        else
        {
            locationPoint.SetActive(false);
        }

        if (Vector3.Distance(this.transform.position,locationPoint.transform.position) < 0.5f)
        {
            setLocationpointbool = false;
            animator.SetBool("DetectiveIdle", true);
            animator.SetBool("DetectiveWalk", false);

        }
        else
        {
            setLocationpointbool = true;

            animator.SetBool("DetectiveWalk", true);
            animator.SetBool("DetectiveIdle", false);

        }

    }

    private void RotateImageDetective()
    {
        Vector3 direction = AgentLocation - this.transform.position;

        if (direction.z  < 0)
        {
            this.gameObject.transform.localScale = new Vector3(1,1,1);
        }
        else
        {
            this.gameObject.transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    bool groundSelect = false;

    private void ClickToMovement()
    {
        groundSelect = true;
        agent.stoppingDistance = 0f;
        setMovementControl = true;
    }

    private void ClickSelection()
    {
        groundSelect = false;
        setMovementControl = true;
    }

    private void CheckDistanceTotarget()  // ingame dialog not activating 
    {
        if (agent.isStopped)
        {
            Debug.Log("yesssss222");
            setactiveMovement = false;
            setLocationpointbool = false;
            canvas_Display.SetActive(true);
            SetStayMovement(false);
            SelectedCharatermulti = InteractionInfo.charaterInteract_Multi;
            MultiDialog = InteractionInfo.SetMultiDialog;
            canvasinteraction.SetDialogType(MultiDialog);

            try
            {
                canvasinteraction.setCharaterDialog(SelectedCharatermulti[SelectedConverstationScript], InteractionInfo.Quest_Information, InteractionInfo);
            }
            catch (Exception)
            {
                canvasinteraction.setCharaterDialog(SelectedCharatermulti[0], InteractionInfo.Quest_Information, InteractionInfo);
            }


            MovementToTarget = false;
        }
    }

    public void SetMovementControl()
    {
        agent.isStopped = false;
        setMovementControl = true;
        canvas_Display.SetActive(false);
        setactiveMovement = true;
    }

    [SerializeField] GameObject tohit;
    private void Objecttohit()
    {
        Vector2 MousePostion = Mouse.current.position.value;
        Ray ray = gameManager._camera.ScreenPointToRay(MousePostion);
        RaycastHit hit;

        if (Mouse.current.leftButton.isPressed)
        {

            if (Physics.Raycast(ray, out hit))
            {

                if (hit.transform.gameObject == tohit.gameObject)
                {
                    //turn light on
                }

            }
        }
    }
}