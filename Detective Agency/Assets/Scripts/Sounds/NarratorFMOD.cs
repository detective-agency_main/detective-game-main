using System.Collections; // Add this line
using UnityEngine;
using FMODUnity;

public class MultiTimedFMODAudio : MonoBehaviour
{
    [System.Serializable]
    public class FMODAudioEvent
    {
        public EventReference eventPath;
        public float delay;
    }

    public FMODAudioEvent[] fmodAudioEvents;
    private FMOD.Studio.EventInstance[] eventInstances;

    void Start()
    {
        eventInstances = new FMOD.Studio.EventInstance[fmodAudioEvents.Length];

        for (int i = 0; i < fmodAudioEvents.Length; i++)
        {
            eventInstances[i] = RuntimeManager.CreateInstance(fmodAudioEvents[i].eventPath);
            StartCoroutine(PlayFMODAudioAfterDelay(i, fmodAudioEvents[i].delay));
        }
    }

    private IEnumerator PlayFMODAudioAfterDelay(int index, float delay)
    {
        yield return new WaitForSeconds(delay);
        eventInstances[index].start();
    }

    void OnDestroy()
    {
        for (int i = 0; i < eventInstances.Length; i++)
        {
            eventInstances[i].release();
        }
    }
}