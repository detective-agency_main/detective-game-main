using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using TMPro;

public class TimedFMODAudioWithUI : MonoBehaviour
{
    [System.Serializable]
    public class FMODAudioEvent
    {
        public EventReference eventPath;
        public float delay;
    }

    public FMODAudioEvent[] fmodAudioEvents;
    private FMOD.Studio.EventInstance[] eventInstances;

    public CanvasGroup uiCanvasGroup;
    public Button continueButton;
    public TMP_Text dialogueText;

    public string dialogueTextToDisplay;
    public float typewriterSpeed = 0.05f;
    public float fadeDuration = 1f;

    private bool playerCanMove = true; 
    private GameObject player;

    void Start()
    {
        eventInstances = new FMOD.Studio.EventInstance[fmodAudioEvents.Length];
        continueButton.onClick.AddListener(CloseUI);
        player = GameObject.FindWithTag("Player");
        StartCoroutine(PlayAudioSequence());
    }

    private IEnumerator PlayAudioSequence()
    {
        if (uiCanvasGroup == null || continueButton == null || dialogueText == null || player == null)
        {
            Debug.LogError("One or more UI elements are not assigned!");
            yield break;
        }

        uiCanvasGroup.alpha = 0;
        yield return StartCoroutine(FadeUI(0, 1));
        
        SetPlayerMovement(false);

        for (int i = 0; i < fmodAudioEvents.Length; i++)
        {
            eventInstances[i] = RuntimeManager.CreateInstance(fmodAudioEvents[i].eventPath);
            eventInstances[i].start();
            yield return new WaitForSeconds(fmodAudioEvents[i].delay);
        }

        yield return StartCoroutine(TypewriterEffect(dialogueTextToDisplay));
        continueButton.gameObject.SetActive(true);
    }

    private IEnumerator TypewriterEffect(string text)
    {
        dialogueText.text = "";
        foreach (char letter in text.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(typewriterSpeed);
        }
    }

    private IEnumerator FadeUI(float startAlpha, float endAlpha)
    {
        float elapsed = 0f;

        uiCanvasGroup.alpha = startAlpha;

        while (elapsed < fadeDuration)
        {
            elapsed += Time.deltaTime;
            uiCanvasGroup.alpha = Mathf.Lerp(startAlpha, endAlpha, elapsed / fadeDuration);
            yield return null;
        }

        uiCanvasGroup.alpha = endAlpha;
    }

    public void CloseUI() 
    {
        StartCoroutine(FadeUI(1, 0));
        continueButton.gameObject.SetActive(false);
        SetPlayerMovement(true);
    }

    private void SetPlayerMovement(bool enabled)
    {
        playerCanMove = enabled;

        if (player.TryGetComponent(out Rigidbody rb))
        {
            rb.isKinematic = !enabled;
        }
    }

    void OnDestroy()
    {
        for (int i = 0; i < eventInstances.Length; i++)
        {
            eventInstances[i].release();
        }
    }
}