using UnityEngine;

public class TimedAudio : MonoBehaviour
{
    public float delay = 5.0f; // Time in seconds before the audio plays
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Invoke("PlayAudio", delay);
    }

    void PlayAudio()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }
}