using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject SelectedCam;
    [SerializeField] GameObject ExitCam_SelectedCam;
    [SerializeField] CameraControl Cam_control;
    [SerializeField] GameObject SpawnPoint;
    Main_playercontrol _Playercontrol;

    private void Start()
    {
        _Playercontrol = FindFirstObjectByType<Main_playercontrol>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(WaittriggerSetActiveCam());
        }
    }

    IEnumerator WaittriggerSetActiveCam()
    {
        yield return new WaitForSeconds(0.8f);
        Cam_control.setActiveCamera(SelectedCam);
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            if (ExitCam_SelectedCam == null)
            {
                return;
            }
            else
            {
                Cam_control.SetBacktoMainCamera(ExitCam_SelectedCam);
            }

            if (SpawnPoint == null)
            {
                return;
            }
            else
            {
                Debug.Log("yes mmove");
                GameObject GOPowerRangers = _Playercontrol.transform.gameObject.GetComponent<GameObject>();
                GOPowerRangers.transform.position = SpawnPoint.transform.localPosition;
            }

        }
    }

}
