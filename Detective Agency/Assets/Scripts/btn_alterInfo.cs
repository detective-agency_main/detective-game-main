using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;


public class btn_alterInfo : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI m_TextMeshPro;
    [SerializeField] public Canvas_Charater_Interaction canvas_inter;
    [HideInInspector] public int SetValueLine = 0;

    public bool Stupid_MeterValue = false;
    public bool Clever_MeterValue = false;

     enum StupidMeter
    {
        SP,
        CP,
        Non
    }

    StupidMeter meterReader = StupidMeter.Non;

    public void SetBtnConvosationClick()
    {

        if (!canvas_inter.DisableClick_OnAudioCheck())
        {
            checkStupidMeterValue();

            switch (meterReader)
            {
                case StupidMeter.SP:
                    canvas_inter.BtnSrollClick(SetValueLine, Canvas_Charater_Interaction.StupidMeter.SP);
                    break;
                case StupidMeter.CP:
                    canvas_inter.BtnSrollClick(SetValueLine, Canvas_Charater_Interaction.StupidMeter.CP);
                    break;
                case StupidMeter.Non:
                    canvas_inter.BtnSrollClick(SetValueLine, Canvas_Charater_Interaction.StupidMeter.Non);
                    break;
                default:
                    break;
            }

            Destroy(this.gameObject);
        }
        
    }

    void checkStupidMeterValue()
    {
        if (!Stupid_MeterValue && !Clever_MeterValue)
        {
            meterReader = StupidMeter.Non;
        }
        else if (Stupid_MeterValue)
        {
            meterReader = StupidMeter.SP;
        }
        else
        {
            meterReader= StupidMeter.CP;
        }
    }
}
