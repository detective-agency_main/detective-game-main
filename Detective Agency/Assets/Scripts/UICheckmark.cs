using UnityEngine;
using UnityEngine.UI;
using DG.Tweening; // Make sure to include this namespace

public class CheckmarkController : MonoBehaviour
{
    public Button button1;
    public Button button2;
    public Button button3;

    public GameObject checkmark1;
    public GameObject checkmark2;
    public GameObject checkmark3;

    // Public variable to adjust initial scale in the Inspector
    public Vector3 initialCheckmarkScale = new Vector3(0.5f, 0.5f, 0.5f); // Default value

    private void Start()
    {
        // Set up button listeners
        button1.onClick.AddListener(() => OnButtonClick(1));
        button2.onClick.AddListener(() => OnButtonClick(2));
        button3.onClick.AddListener(() => OnButtonClick(3));

        // Initialize checkmarks
        checkmark1.SetActive(false);
        checkmark2.SetActive(false);
        checkmark3.SetActive(false);
    }

    private void OnButtonClick(int buttonIndex)
    {
        // Reset all checkmarks
        HideCheckmark(checkmark1);
        HideCheckmark(checkmark2);
        HideCheckmark(checkmark3);

        // Show the selected checkmark
        switch (buttonIndex)
        {
            case 1:
                ShowCheckmark(checkmark1);
                break;
            case 2:
                ShowCheckmark(checkmark2);
                break;
            case 3:
                ShowCheckmark(checkmark3);
                break;
        }
    }

    private void ShowCheckmark(GameObject checkmark)
    {
        checkmark.SetActive(true);
        checkmark.transform.localScale = initialCheckmarkScale; // Use the inspector value

        // Animate the checkmark with DoTween
        checkmark.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBounce); // Adjust time and easing as needed
    }

    private void HideCheckmark(GameObject checkmark)
    {
        if (checkmark.activeSelf)
        {
            // Animate the checkmark to scale down and then deactivate it
            checkmark.transform.DOScale(Vector3.zero, 0.3f).SetEase(Ease.InBack).OnComplete(() => checkmark.SetActive(false));
        }
    }
}