using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using static UnityEngine.InputSystem.InputAction;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour, ISaveable
{
    [SerializeField] public Camera _camera;
    [SerializeField] GameObject Jour_canvas;
    [SerializeField] GameObject Pause_canvas;
    [SerializeField] GameObject Inventory_canvas;
    Main_playercontrol _playercontrol;

    [SerializeField] public Quest_Information_Holder[] SceneQuestInformation;
    [SerializeField] Canvas_Charater_Interaction Canvas_Interaction;

    [SerializeField] public Object_InventoryImport[] ObjectData;
    [SerializeField] PlayableDirector PlayerTimeline;

    public bool setconversation { get; set; }
    int currentScene;
    // Start is called before the first frame update
    void Start()
    {
        setconversation = true;
        Jour_canvas.SetActive(false);
        Pause_canvas.SetActive(false);
        Inventory_canvas.SetActive(false);
        checkScene();

        _playercontrol = FindFirstObjectByType<Main_playercontrol>();

        try
        {
            foreach (var item in ObjectData)
            {
                if (item.ObjectCollected)
                {
                    Canvas_Interaction.LoadIntoEvidence(item);
                }
            }
        }
        catch (System.Exception)
        {
            throw;
        }
    }

    // Update is called once per frame
    

    bool BtnJournal = false;
    public void OnJornal(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            BtnJournal = BtnJournal ? false : true;
            Jour_canvas.SetActive(BtnJournal);
            _playercontrol.StopMovementState_Menu(!BtnJournal);
           // Time.timeScale = BtnJournal ? 0 : 1;
        }
    }

    public void OnJornal()
    {
            BtnJournal = BtnJournal ? false : true;
            Jour_canvas.SetActive(BtnJournal);
            _playercontrol.StopMovementState_Menu(!BtnJournal);
        //  Time.timeScale = BtnJournal ? 0 : 1;
    }
    bool btnInvotory = false;
    bool BtnLeftClick = false;

    public void OnLeftClick(InputAction.CallbackContext callback)
    {
        if (callback.performed)
        {
            BtnLeftClick = callback.performed;

            _playercontrol.CheckClickAction();
        }
    }

    public void OnRightClick(InputAction.CallbackContext ctx) 
    {
        if (ctx.performed)
        {
            Debug.Log("RightClick");
            Canvas_Interaction.CallNextLine();
        }

    }

    public void Oninventory()
    {
        btnInvotory = btnInvotory ? false : true;
        Inventory_canvas.SetActive(btnInvotory);
        _playercontrol.StopMovementState_Menu(!btnInvotory);
        //  Time.timeScale = BtnJournal ? 0 : 1;
    }

    bool BtnPause = false;
    public void OnPause(InputAction.CallbackContext callbackContext)
    {

        if (callbackContext.performed)
        {
            BtnPause = BtnPause ? false : true;
            Pause_canvas.SetActive(BtnPause);
            _playercontrol.StopMovementState_Menu(!BtnPause);
        }
    }

    public void OnInventory(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            btnInvotory = btnInvotory ? false : true;
            Inventory_canvas.SetActive(btnInvotory);
            _playercontrol.StopMovementState_Menu(!btnInvotory);
        }
    }

    bool questbarDis = false;
    public void OnQuests(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            questbarDis = questbarDis ? false : true;

            if (questbarDis)
            {
                Canvas_Interaction.showQuestBar();
            }
            else
            {
                Canvas_Interaction.HideQuestBar();
            }
        }
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void checkScene()
    {

        foreach (var item in SceneQuestInformation)
        {
            item.QuestComplete = false;
        }

        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            foreach (var item in SceneQuestInformation)
            {
                item.QuestComplete = false;
            }

            foreach (var item in ObjectData)
            {
                item.ObjectCollected = false;
            }
        }
    }

    public void LoadPreviousLevel()
    {
        
    }

    public void ChangeQuestListHolder(string QuestName)
    {
        foreach (var item in SceneQuestInformation)
        {
            if (item.QuestName == QuestName)
            {
                Destroy(item);
            }
        }
    }
    int QuestCount = 0;
    int QuestIndex_Number;
    bool SceneQuests_Completed = false;
    QuestCheck_InfoGenorator infoGenoratorCheck;
    public void ChangeQuestInfo(string QuestName)
    {
        QuestIndex_Number = SceneQuestInformation.Length;

        foreach (var item in SceneQuestInformation)
        {
            if (item.QuestName == QuestName)
            {
                if (item.DialogOrFind)
                {
                    QuestCount++; // find Objects clues
                    item.QuestComplete = true;

                }
                else
                {
                    QuestCount++; // talking to strangers
                    item.QuestComplete = true;

                    _playercontrol.SetStayMovement(true);
                }
            }
        }

        if (QuestCount == QuestIndex_Number) // Quest Trigger Complete for the scene
        {
            QuestCount = 0;
            SceneQuests_Completed = true;

            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Foyer"))
            {
                Debug.Log("Change Scenes");
            }

            Canvas_Interaction.TriggerQuestsComplete_screen();

            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Foyer"))
            {
                Invoke("SetNarrativeTimeLine",3);
            }
        }

        var FoundQuestInfo = GameObject.FindGameObjectsWithTag("QuestListHolder");

        foreach (var Quests in FoundQuestInfo)
        {
            infoGenoratorCheck = Quests.GetComponent<QuestCheck_InfoGenorator>();
            infoGenoratorCheck.CheckInfoStatus();
        }
        

    }

    private void SetNarrativeTimeLine()
    {
        Debug.Log("Playing");
        PlayerTimeline.Play();
    }

    // save and loading 
    const string savedata = "SaveData.bat";
    private static void SaveJsonData(GameManager gameManager)
    {
        DetectiveSaveData saveData = new DetectiveSaveData();
        gameManager.PopulateSaveData(saveData);

        if (FileManager.WriteToFile(savedata, saveData.ToJason()))
        {
            Debug.Log("save successful");
        }

    }

    public void PopulateSaveData(DetectiveSaveData saveData)
    {
        saveData.LoadScene = SceneManager.GetActiveScene().buildIndex;
    }

    private static void LoadJsonData(GameManager gameManager) 
    {
        if (FileManager.LoadFromFile(savedata,out var jason))
        {
            DetectiveSaveData saveData = new DetectiveSaveData();
            saveData.LoadFromJason(jason);

            gameManager.LoadFromSaveData(saveData);
            Debug.Log("load successful");
        }
    }

    public  void LoadFromSaveData(DetectiveSaveData saveData)
    {
        currentScene = saveData.LoadScene;

    }

}
