using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;
using FMODUnity;
using FMODUnityResonance;
using FMOD.Studio;
using System;
/*using static UnityEditor.Profiling.RawFrameDataView;*/
using System.Runtime.InteropServices;

public class AudioManager : MonoBehaviour
{
    public bool OnceOff = false;
    private EventInstance eventInstance;
    private EventInstance DialogEvent;
    private EventInstance DialogEventHint;
    EventInstance Del_AudioSelect;

    [SerializeField] public EventReference fmodEvent;

    private FMOD.DSP fftDsp;
    private float[] spectrumData = new float[512]; // Array for storing spectrum data
    private FMOD.DSP mFFT;
    private float[] mFFTSpectrum;
    const int WindowSize = 1024;
    FMOD.Studio.EventInstance musicInstance;
    FMOD.DSP fft;

    const float WIDTH = 10.0f;
    const float HEIGHT = 0.1f;
    Vector3 spectrumPos = Vector3.zero;

    string spectrumValue_str = "";
    public bool AudioIsplaying = false;
    bool SetrBool = false;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        // Optionally, start playing the event
        eventInstance.start();
        DialogEvent.start();
        Del_AudioSelect.start();

    }

    public void PlayOneShot(EventReference sound,Vector3 worldpos)
    {
        if (!OnceOff) 
        {
            //eventInstance = RuntimeManager.CreateInstance(sound);
            RuntimeManager.PlayOneShot(sound, worldpos);
            eventInstance.start();
            OnceOff = true;
        }
        StartCoroutine(changeboolean());
    }

    public void StopAudio(EventInstance selectedDelAudio)
    {
        selectedDelAudio.release();
        selectedDelAudio.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    
    public EventInstance GetInstanceDialog()
    {
        return DialogEvent;
    }

    public void DialogAudioPlayHint(EventReference SelectedAudio)
    {
        DialogEventHint = RuntimeManager.CreateInstance(SelectedAudio);
        DialogEventHint.start();
    }

    void OnDestroy()
    {
        // Clean up the event instance
        eventInstance.release();
    }

    IEnumerator changeboolean()
    {
        yield return new WaitForSeconds(1);
        OnceOff = false;
    }

    public bool IsPlaying()
    {
        FMOD.Studio.PLAYBACK_STATE state;
        DialogEvent.getPlaybackState(out state);
        return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
    }

    
    private void Update()
    {
        if (IsPlaying() && SetrBool)
        {
            SetSpectrumData();
        }
    }

    public void DialogAudioPlay(EventReference SelectedAudio)
    {
        DialogEvent = RuntimeManager.CreateInstance(SelectedAudio);
        SpectrumData();
        DialogEvent.start();
    }

    private void SpectrumData()
    {
        FMODUnity.RuntimeManager.CoreSystem.createDSPByType(FMOD.DSP_TYPE.FFT, out fft);

        fft.setParameterInt((int)FMOD.DSP_FFT.WINDOWTYPE, (int)FMOD.DSP_FFT_WINDOW.HANNING);
        fft.setParameterInt((int)FMOD.DSP_FFT.WINDOWSIZE, WindowSize * 2);

        FMOD.ChannelGroup channelGroup;
        FMODUnity.RuntimeManager.CoreSystem.getMasterChannelGroup(out channelGroup);
        channelGroup.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD, fft);
        SetrBool = true;
    }

    float[][] SpectrumDataArray;

    private void SetSpectrumData()
    {
        IntPtr unmanagedData;
        uint length;
        fft.getParameterData((int)FMOD.DSP_FFT.SPECTRUMDATA, out unmanagedData, out length);
        FMOD.DSP_PARAMETER_FFT fftData = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(unmanagedData, typeof(FMOD.DSP_PARAMETER_FFT));
        var spectrum = fftData.spectrum;

        if (fftData.numchannels > 0)
        {
            var pos = Vector3.zero;
            pos.x = WIDTH * -0.5f;

            for (int i = 0; i < WindowSize; ++i)
            {
                pos.x += (WIDTH / WindowSize);
                float level = lin2dB(spectrum[0][i]);

                pos.y = (80 + level) * HEIGHT;
                spectrumPos = pos;

                //UnityEngine.Debug.Log(spectrum[0][i].ToString());
                spectrum_stuff(spectrum[0][i].ToString());
            }

        }
    }

    private void spectrum_stuff(string spectrumvalue)
    {
        spectrumValue_str = spectrumvalue;
    }
   
    public string GetSpectrumstuff()
    {
        return spectrumValue_str;
    }

    float lin2dB(float linear)
    {
        return Mathf.Clamp(Mathf.Log10(linear) * 20.0f, -80.0f, 80.0f);
    }

}

