using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.InputSystem;
using DG.Tweening.Core.Easing;

public class Main_Menu_Canvas : MonoBehaviour
{
    [Header("Normal")]
    [SerializeField] GameObject StartMenu;
    [SerializeField] GameObject OptionsMenu;
    [SerializeField] GameObject OpenMenu;

    [Header("UIControl Front")]
    [SerializeField] GameObject enOnvelop;

    [Header("ImagesEnvelop")]
    [SerializeField] GameObject LidEnvolop;
    [SerializeField] GameObject TextAccept;

    [Header("Old Images To Disappear")]
    [SerializeField] List<GameObject> oldImages;
    [SerializeField] GameObject delayedImage; // The image to delay and slide out

    [Header("Options")]
    [SerializeField] GameObject Optimages;
    [SerializeField] GameObject OptcontextPanel;

    [Header("Options")]
    [SerializeField] Camera MainMenuCam;

    [SerializeField] GameObject[] Letters;

    private void Awake()
    {
        foreach (GameObject go in Letters)
        {
            go.SetActive(false);

        }

        int randomVal = Random.Range(0, Letters.Length);

        Letters[randomVal].SetActive(true);
    }

    void Start()
    {
        StartMenu.transform.Rotate(new Vector3(0, 90, 0));
        StartMenu.SetActive(false);
        OptionsMenu.SetActive(false);
        Optimages.transform.DOScale(0f, 0.1f);
        OptcontextPanel.transform.position = new Vector3(OptcontextPanel.transform.position.x, OptcontextPanel.transform.position.y - 867, 0);

    }

    public void OnLeftClick(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            Vector2 MousePostion = Mouse.current.position.value;
            Ray ray = MainMenuCam.ScreenPointToRay(MousePostion);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag == "Letter")
                {
                    OpenMenu.SetActive(true);
                    enOnvelop.transform.DOScale(1f, 1f).SetEase(Ease.InOutBounce);
                }
            }
        }
    }

    public void StartBtn()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void OpenOptionsbtn()
    {
        StartMenu.SetActive(false);
        OptionsMenu.SetActive(true);
        Optimages.transform.DOScale(1f, 1f);
        OptcontextPanel.transform.DOMoveY(390, 1f);
    }

    public void CloseOptions()
    {
        StartMenu.SetActive(true);
        OptionsMenu.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OpenEnvolop()
    {
        foreach (GameObject image in oldImages)
        {
            if (image != delayedImage)
                image.SetActive(false);
        }

        Invoke("HideDelayedImage", 1.5f); 

        LidEnvolop.transform.DORotate(new Vector3(150, 0, 0), 1f);
        enOnvelop.transform.DOScale(3.1f, 1f);
        TextAccept.SetActive(false);
        Invoke("OpenLetter", 1);
    }

    void HideDelayedImage()
    {
        
        delayedImage.transform.DOMoveX(-500, 1f).OnComplete(() => delayedImage.SetActive(false));
    }

    public void OpenLetter()
    {
        StartMenu.SetActive(true);
        OptionsMenu.SetActive(false);
        StartMenu.transform.DORotate(new Vector3(0, 0, 0), 1f);
    }
}