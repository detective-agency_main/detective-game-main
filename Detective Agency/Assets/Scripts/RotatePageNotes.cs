using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class RotateOnClick : MonoBehaviour, IPointerClickHandler
{
    public GameObject textObject; // Reference to the text GameObject
    public float rotationSpeed = 50f; // Rotation speed

    private bool isRotating = false; // Flag to track if the object is currently rotating
    private bool isRotated = false;  // Flag to track if the object has rotated

    // Called when the object is clicked
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isRotating)
        {
            // Determine the target rotation angle based on the current state
            float targetAngle = isRotated ? 0f : 180f;
            
            // Rotate the GameObject to the target angle using DoTween
            RotateTo(targetAngle);
        }
    }

    // Method to rotate the GameObject to a specific angle using DoTween
    private void RotateTo(float angle)
    {
        isRotating = true;

        // Deactivate the text GameObject during rotation
        textObject.SetActive(false);

        transform.DORotate(new Vector3(0f, angle, 0f), 1f, RotateMode.LocalAxisAdd)
                 .SetEase(Ease.Linear)
                 .OnComplete(() =>
                 {
                     // Rotation completed
                     isRotating = false; // Reset rotation flag
                     isRotated = !isRotated; // Toggle rotation state

                     // Reactivate the text GameObject after rotation is completed
                     textObject.SetActive(true);
                 });
    }
}