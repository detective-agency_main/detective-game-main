using UnityEngine;
using UnityEngine.UI;
using DG.Tweening; // DoTween

public class ScaleOpenEffect : MonoBehaviour
{
    public RectTransform imageRect; 
    public float scaleDuration = 1f; 

    void Start()
    {
        
        imageRect.localScale = new Vector3(1, 0, 1);

       
        ScaleOpen();
    }

    public void ScaleOpen()
    {
       
        imageRect.DOScaleY(1, scaleDuration)
                 .SetEase(Ease.OutQuad); 
    }
}