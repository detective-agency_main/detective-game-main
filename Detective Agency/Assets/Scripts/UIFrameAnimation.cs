using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ButtonFrameEffect : MonoBehaviour
{
    public Button button;           // Reference to the button
    public Image frame;             // Reference to the single frame (Image component)
    public Color frameHoverColor = Color.white;  // Color on hover
    public Color frameSelectColor = Color.yellow;  // Color on selection
    private Vector2 originalSize;   // Original size for the frame

    void Start()
    {
        originalSize = frame.rectTransform.sizeDelta;
        SetFrameSize(Vector2.zero); // Start with no visible frame

        // Add button click listener
        button.onClick.AddListener(OnButtonClick);
    }

    // Set the size of the frame
    private void SetFrameSize(Vector2 size)
    {
        frame.rectTransform.sizeDelta = size;
    }

    // Call this from Unity's EventTrigger for pointer enter
    public void OnMouseEnter()
    {
        // Animate frame size to create the drawing effect
        SetFrameSize(new Vector2(0, 0)); // Reset size before starting animation
        frame.rectTransform.DOSizeDelta(new Vector2(originalSize.x, originalSize.y), 0.3f).From(new Vector2(0, 0));
        frame.color = frameHoverColor;  // Set hover color
    }

    // Call this from Unity's EventTrigger for pointer exit
    public void OnMouseExit()
    {
        // Fade out the frame
        frame.DOFade(0, 0.3f).OnComplete(() => SetFrameSize(Vector2.zero));
    }

    public void OnButtonClick()
    {
        // Change the frame color to indicate selection
        frame.color = frameSelectColor; 
    }

    public void ResetFrame()
    {
        SetFrameSize(Vector2.zero);  // Reset frame size when returning to the menu
    }
}