using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class petcontroller : MonoBehaviour
{
    NavMeshAgent agent;
    [SerializeField] GameObject Main_Player;
    [SerializeField] bool AreaStay = true;
    [SerializeField] GameObject[] HintSectionPoints;
    Transform[] HintSectionPointsChildren;
    AudioManager Audiomanager;
    [HideInInspector] public Trigger_Player_assistent player_Assistent;
    int inccount = 0;
    bool onceClick = true;
    bool onceaudio = false;
    Animator animatorliya;
    [SerializeField] List<EventReference> HintSounds = new List<EventReference>();

    private enum WalkingState // make sure the corrospond on trigger assist
    {
        PotrolHints,
        Stay,
        followDetective
    }

    [SerializeField] private WalkingState walkingState = WalkingState.Stay;

    

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Audiomanager = FindFirstObjectByType<AudioManager>();
        animatorliya = GetComponent<Animator>();
    }

    Vector3 NewLocation = Vector3.zero;
    int CountSelectedpoint = 0;
    bool setWalkstateHints = true;
    bool setusingpoints = false;
    float countStay_Hints = 0;
    float RandUse;
    void Update()
    {
        switch (walkingState)
        {
            case WalkingState.PotrolHints:

                agent.isStopped = false;

                agent.stoppingDistance = 0.1f;

                if (agent.remainingDistance < 0.4f)
                {
                    CountSelectedpoint++;
                    walkingState = WalkingState.Stay;

                    animatorliya.SetBool("walk", false);
                    animatorliya.SetBool("idle", true);

                    RandUse = Random.Range(5, 15);

                    if (RandUse > 7)
                    {
                        Audiomanager.DialogAudioPlayHint(HintSounds[Random.Range(0,HintSounds.Count)]);
                    }
                    countStay_Hints = 0;

                    CountSelectedpoint = (CountSelectedpoint == HintSectionPointsChildren.Length) ? 0 : CountSelectedpoint;
                }

                agent.destination = HintSectionPointsChildren[CountSelectedpoint].transform.position;
                
                break;
            case WalkingState.Stay:

                agent.isStopped = true;

                if (setusingpoints)
                {
                    countStay_Hints += Time.deltaTime;

                    if (countStay_Hints > RandUse)
                    {


                        walkingState = WalkingState.PotrolHints;

                        animatorliya.SetBool("idle", false);
                        animatorliya.SetBool("walk", true);

                        RandUse = 0;
                        countStay_Hints = 0;
                    }
                }

                break;
            case WalkingState.followDetective:

                agent.isStopped = false;

                agent.stoppingDistance = 3.0f;
                agent.destination = Main_Player.transform.position;

                if (agent.stoppingDistance < 2.5) 
                {
                    animatorliya.SetBool("walk", false);
                    animatorliya.SetBool("idle", true);
                }
                else
                {
                    animatorliya.SetBool("idle", false);
                    animatorliya.SetBool("walk", true);
                }

            break;
        }
    }

    public void SetWalkingState(Trigger_Player_assistent.WalkingState state)
    {

        switch (state)
        {
            case Trigger_Player_assistent.WalkingState.PotrolHints:
                walkingState = WalkingState.PotrolHints;
                setusingpoints = true;
                break;
            case Trigger_Player_assistent.WalkingState.Stay:
                walkingState = WalkingState.Stay;
                setusingpoints = false;
                break;
            case Trigger_Player_assistent.WalkingState.followDetective:
                walkingState = WalkingState.followDetective;
                setusingpoints = false;
                break;
        }
    }

    public void ChangetoMovestate()
    {
        walkingState = WalkingState.followDetective;
    }

    public void setSelected_ArrayPoints(int selectedHintsGroupInt)
    {
        HintSectionPointsChildren = new Transform[HintSectionPoints[selectedHintsGroupInt].transform.childCount];
        setUpArrayPoints(selectedHintsGroupInt);
    }

    private void setUpArrayPoints(int selectedHintsGroup)
    {
        for (int i = 0; i < HintSectionPointsChildren.Length; i++)
        {
            HintSectionPointsChildren[i] = HintSectionPoints[selectedHintsGroup].transform.GetChild(i).GetComponent<Transform>();
        }

    }

    public void stayState()
    {
        AreaStay = AreaStay ? true : false; 
    }
}
