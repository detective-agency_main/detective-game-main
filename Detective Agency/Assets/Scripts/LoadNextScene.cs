using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour
{
    public string Scene;
    public CapsuleCollider Player;
    [SerializeField] private Quest_Information_Holder _infoQuestBulter;

    private void OnTriggerEnter(Collider other)
    {
        if (other == Player)
        {

            if (Scene == "VictoriaRoom")
            {
                SceneManager.LoadScene(Scene);
                Debug.Log("Scene is loaded");

                //if (_infoQuestBulter.QuestComplete)
                //{
                //    SceneManager.LoadScene(Scene);
                //    Debug.Log("Scene is loaded");
                //}
                //else
                //{
                //    Debug.Log("Talk to butler");
                //}

            }
            else
            {
                SceneManager.LoadScene(Scene);
                Debug.Log("Scene is loaded");
            }
        }
    }




}
