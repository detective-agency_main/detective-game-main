using UnityEngine;
using UnityEngine.InputSystem;

public class PushTray : MonoBehaviour
{
   
    GameManager gameManager;
    [SerializeField] Rigidbody rb;
    [SerializeField] Camera Cam;
    [SerializeField] GameObject GO;
    [SerializeField] float pushforce;
    public DetectiveAgency Inputs;


    private void Update()
    {
        Cam = gameManager._camera;
    }

    private void Awake()
    {
        gameManager = FindFirstObjectByType<GameManager>();
        Inputs = new DetectiveAgency();
        Inputs.Enable();
        Inputs.Player.LeftClick.performed += OnClick;
        Inputs.Player.LeftClick.canceled += OnClick;


        void OnClick(InputAction.CallbackContext context)
        {

            if (context.performed)
            {
                Vector2 MousePostion = Mouse.current.position.value;
                Ray ray = Cam.ScreenPointToRay(MousePostion);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.gameObject == GO)
                    {
                        rb.AddRelativeForce(0,0,pushforce,ForceMode.Impulse);
                       
                    }
                }

            }
        }
    }






}
