using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Opendoor : MonoBehaviour
{
    public Transform LeftDoor;
    public Transform RightDoor;
    public float DoorSpeed;
    [SerializeField] public float DoorMove;

    Vector3 startingPointLeft;
    Vector3 startingPointRight;

    float timemain = 0;
    bool starttrigger = false;
    bool ChangeDirection = false;
    private void Start()
    {
        startingPointLeft = LeftDoor.position;
        startingPointRight = RightDoor.position;
    }

    void Update()
    {

        if (starttrigger)
        {

            if (ChangeDirection)
            {
                Vector3 vector3Left = startingPointLeft - (Vector3.right * DoorMove);
                LeftDoor.transform.position = Vector3.Lerp(startingPointLeft, vector3Left, timemain);

                Vector3 vector3Right = startingPointRight + (Vector3.right * DoorMove);
                RightDoor.transform.position = Vector3.Lerp(startingPointRight, vector3Right, timemain);

                timemain += Time.deltaTime * DoorSpeed;
            }
            else
            {
                Vector3 vector3Left = startingPointLeft - (Vector3.right * DoorMove);
                LeftDoor.transform.position = Vector3.Lerp(vector3Left, startingPointLeft, timemain);

                Vector3 vector3Right = startingPointRight + (Vector3.right * DoorMove);
                RightDoor.transform.position = Vector3.Lerp(vector3Right, startingPointRight, timemain);

                timemain += Time.deltaTime * DoorSpeed;
            }

        }
        

    }


    private void OnTriggerEnter(Collider other)
    {
        timemain = 0;
        starttrigger = true;
        ChangeDirection = true;
    }
    private void OnTriggerExit(Collider other)
    {
        timemain = 0;
        ChangeDirection = false;
        StartCoroutine(EndDoorTriggerwait());
    }

    IEnumerator EndDoorTriggerwait()
    {
        yield return new WaitForSeconds(5f);
        starttrigger = false;
    }

}
