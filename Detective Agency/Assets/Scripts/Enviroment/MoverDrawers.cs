using DG.Tweening;
using DG.Tweening.Core.Easing;
using UnityEngine;
using UnityEngine.InputSystem;

public class MoverDrawers : MonoBehaviour
{
    GameManager gameManager;
    [SerializeField] bool DrawerisOpen;
    [SerializeField] Camera Cam;
    private bool LeftClickedInput;
    public DetectiveAgency Inputs;
    [SerializeField] float drawerSpeed;
    [SerializeField] float drawerEndValue;
    [SerializeField] float drawerStartValue;



    
    private void Awake()
    {
        gameManager = FindFirstObjectByType<GameManager>();

        Inputs = new DetectiveAgency();
        Inputs.Enable();
        Inputs.Player.LeftClick.performed += OnClick;
        Inputs.Player.LeftClick.canceled += OnClick;

        void OnClick(InputAction.CallbackContext context)
        {

            if (context.performed)
            {
                Vector2 MousePostion = Mouse.current.position.value;
                Ray ray = Cam.ScreenPointToRay(MousePostion);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.gameObject == this.gameObject && !DrawerisOpen)
                    {
                        OpenDrawer();
                    }

                    else if (hit.transform.gameObject == this.gameObject && DrawerisOpen)
                    {

                        CloseDrawer();
                    }
                }
            }
        }
    }

   


    public void OpenDrawer()
    {
        this.transform.DOLocalMoveZ(drawerEndValue,drawerSpeed);
        DrawerisOpen = true;
    }

    public void CloseDrawer()
    {
        this.transform.DOLocalMoveZ(drawerStartValue, drawerSpeed);
        DrawerisOpen = false;
    }

}
