
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayAnimations : MonoBehaviour
{
    GameManager gameManager;
    [SerializeField] string AnimationName;
    [SerializeField] Camera Cam;
    [SerializeField] bool AnimationisPlaying;
    [SerializeField] Animator animator;
    [SerializeField] GameObject GO;
    public DetectiveAgency Inputs;


    private void Update()
    {
        Cam = gameManager._camera;
    }

    private void Awake()
    {
        gameManager = FindFirstObjectByType<GameManager>();
        Inputs = new DetectiveAgency();
        Inputs.Enable();
        Inputs.Player.LeftClick.performed += OnClick;
        Inputs.Player.LeftClick.canceled += OnClick;


        void OnClick(InputAction.CallbackContext context)
        {

            if (context.performed)
            {
                Vector2 MousePostion = Mouse.current.position.value;
                Ray ray = Cam.ScreenPointToRay(MousePostion);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.gameObject == GO)
                    {
                        PlayAnim();
                    }
                }

            }
        }
    }

    public void PlayAnim()
    {
     
        animator.SetTrigger(AnimationName);
    }

}
