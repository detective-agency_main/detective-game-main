using DG.Tweening.Core.Easing;
using UnityEngine;
using UnityEngine.InputSystem;

public class TurnLightsOnorOff : MonoBehaviour
{
    GameManager gameManager;
    [SerializeField] GameObject Light;
    [SerializeField] bool lightisOn;
    [SerializeField] Camera Cam;
    private bool LeftClickedInput;
    public DetectiveAgency Inputs;

    private void Update()
    {
    }
    private void Awake()
    {
        gameManager = FindFirstObjectByType<GameManager>();
        Inputs = new DetectiveAgency();
        Inputs.Enable();
        Inputs.Player.LeftClick.performed += OnClick;
        Inputs.Player.LeftClick.canceled += OnClick;

    }

  /*  private void Update()
    {
        Debug.Log(lightisOn);
    }*/
    void OnClick(InputAction.CallbackContext context)
    {

        if (context.performed)
        {
            Vector2 MousePostion = Mouse.current.position.value;
            Ray ray = Cam.ScreenPointToRay(MousePostion);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject == this.gameObject && !lightisOn)
                {
                    TurnLightOn();
                }

                else if (hit.transform.gameObject == this.gameObject && lightisOn)
                {
                    TurnLightOff();
                   
                }
            }
        }
    }

    public void TurnLightOn()
    {
        Light.gameObject.SetActive(true);
        lightisOn=true;
    }

    public void TurnLightOff()
    {
        Light.gameObject.SetActive(false);
        lightisOn = false;
    }

}

