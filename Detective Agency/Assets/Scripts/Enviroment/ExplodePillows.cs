using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ExplodePillows : MonoBehaviour
{
    GameManager gameManager;
    [SerializeField] Camera Cam;
    private bool LeftClickedInput;
    public DetectiveAgency Inputs;
    public List<Rigidbody> RbList;
    [SerializeField] float explotionForce;
    [SerializeField] float explotionRadius;
    [SerializeField] float upwardsModifier;

   
    private void Update()
    {
    }

    private void Awake()
    {
        gameManager = FindFirstObjectByType<GameManager>();
        Inputs = new DetectiveAgency();
        Inputs.Enable();
        Inputs.Player.LeftClick.performed += OnClick;
        Inputs.Player.LeftClick.canceled += OnClick;
       


        void OnClick(InputAction.CallbackContext context)
        {

            if (context.performed)
            {
                Vector2 MousePostion = Mouse.current.position.value;
                Ray ray = Cam.ScreenPointToRay(MousePostion);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.gameObject == this.gameObject)
                    {
                   
                            for(int i = 0; i < RbList.Count; i++)
                            {
                            if (i >= 0)
                            {
                                RbList[i].isKinematic = false;
                                RbList[i].AddExplosionForce(explotionForce, this.transform.position, explotionRadius,upwardsModifier);

                            }
                             gameObject.SetActive(false);

                        }
                       
                    }

                }
            }
        }

    }
}
