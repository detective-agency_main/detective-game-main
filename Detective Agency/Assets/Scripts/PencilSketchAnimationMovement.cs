using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PencilSketchAnimation : MonoBehaviour
{
    public Image sketchImage; // Assign your UI Image in the Inspector
    public float movementAmount = 2f; // Smaller movement amount for a subtle effect
    public float animationDuration = 1f; // Duration of the animation

    private Vector3 originalPosition;

    private void Start()
    {
        // Store the original position
        originalPosition = sketchImage.transform.localPosition;

        // Start the animation loop
        AnimateImage();
    }

    private void AnimateImage()
    {
        // Create a sequence for the animation
        Sequence sequence = DOTween.Sequence();

        // Move the image to the right
        sequence.Append(sketchImage.transform.DOLocalMoveX(originalPosition.x + movementAmount, animationDuration)
            .SetEase(Ease.InOutSine));

        // Move the image back to the original position
        sequence.Append(sketchImage.transform.DOLocalMoveX(originalPosition.x, animationDuration)
            .SetEase(Ease.InOutSine));

        // Set the sequence to loop indefinitely
        sequence.SetLoops(-1, LoopType.Yoyo);
    }
}