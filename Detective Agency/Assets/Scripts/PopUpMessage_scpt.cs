using FMODUnity;
using UnityEngine;
/*using static UnityEditor.PlayerSettings;*/

public class PopUpMessage_scpt : MonoBehaviour
{
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    [SerializeField] GameObject PlayerPosition;
    [SerializeField] float amplitude = 1; 
    [SerializeField] float frequency = 2; 
    [SerializeField] float PositionValue = 2;
    [SerializeField] float OverAllScale = 1;
    [SerializeField] float PlayerDistance = 15f;
    [SerializeField] EventReference CloseUpSound;
    float playerdistance = 0;
    AudioManager audioManager;
    void Start()
    {
        this.transform.localScale = Vector3.zero;
        audioManager = FindFirstObjectByType<AudioManager>();
    }

    bool onceoff = true;
    // Update is called once per frame
    void Update()
    {
        CheckPlayerDistance();

        if (playerdistance > PlayerDistance)
        {
            if (onceoff)
            {
                onceoff = onceoff ? false : true;
                audioManager.PlayOneShot(CloseUpSound, this.transform.position);
                this.transform.localScale = Vector3.one;
            }
        }
        else
        {
            onceoff = onceoff ? false : true;
            this.transform.localScale = Vector3.zero;
        }

        float sinRightValue = (Mathf.Sin(Time.time * amplitude) * frequency) + PositionValue;
        Vector3 FullScale = new Vector3(sinRightValue * OverAllScale, sinRightValue * OverAllScale, sinRightValue * OverAllScale);
        this.transform.localScale = FullScale;

    }

    private void CheckPlayerDistance()
    {
        playerdistance = Vector3.Distance(this.transform.position, PlayerPosition.transform.position);
    } 
}
